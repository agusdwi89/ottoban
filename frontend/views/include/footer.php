<?
	$fabout 		= $this->db->get_where('gs_config',array('cog_id'=>2))->row()->cog_value;
	$fcontact 		= $this->db->get_where('gs_config',array('cog_id'=>3))->row()->cog_value;
	$last_berita 	= $this->db->order_by('berita_id','desc')->get('gs_berita',5);
	$last_artikel 	= $this->db->order_by('id','desc')->get('gs_artikel',5);
	$sco = $this->db->get_where('gs_social',array('soc_st'=>1))->result();
?>

<footer class="site-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="footer-widget">
					<h4 class="footer-widget-title">Statistik Website</h4>
					<div>
						<!-- Histats.com  (div with counter) --><div id="histats_counter"></div>
						<!-- Histats.com  START  (aync)-->
						<script type="text/javascript">var _Hasync= _Hasync|| [];
						_Hasync.push(['Histats.start', '1,2621342,4,3026,130,80,00001011']);
						_Hasync.push(['Histats.fasi', '1']);
						_Hasync.push(['Histats.track_hits', '']);
						(function() {
							var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
							hs.src = ('http://s10.histats.com/js15_as.js');
							(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
						})();</script>
						<noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?2621342&101" alt="counter statistics" border="0"></a></noscript>
						<!-- Histats.com  END  -->
					</div>
					<ul class="footer-media-icons" style="margin-left:8px;margin-top:10px">
						<li><a href="<?=$sco[0]->soc_link;?>" class="fa fa-facebook"></a></li>
						<li><a href="<?=$sco[1]->soc_link;?>" class="fa fa-twitter"></a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3">
				<div class="footer-widget">
					<h4 class="footer-widget-title">Artikel</h4>
					<ul class="list-links">
						<?foreach ($last_artikel->result() as $key): ?>
							<li><a href="<?=base_url('artikel/read/'.$key->link);?>"><?=$key->judul;?></a></li>	
						<?endforeach;?>
					</ul>
				</div>
			</div>
			<div class="col-md-3">
				<div class="footer-widget">
					<h4 class="footer-widget-title">Berita</h4>
					<ul class="list-links">
						<?foreach ($last_berita->result() as $key): ?>
							<li><a href="<?=base_url('berita/read/'.$key->berita_link);?>"><?=$key->berita_judul;?></a></li>	
						<?endforeach;?>
					</ul>
				</div>
			</div>
			<div class="col-md-3">
				<div class="footer-widget">
					<h4 class="footer-widget-title">Informasi</h4>
					<ul class="list-links">
						<li><a href="<?=base_url()?>site/pembaca">Pembaca</a></li>
						<li><a href="<?=base_url()?>site/penulis">Penulis</a></li>
						<li><a href="<?=base_url()?>site/pustakawan">Pustakawan</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="bottom-footer">
			<div class="row">
				<div class="col-md-5">
					<p class="small-text">&copy; Copyright 2014. Devoloped by <a href="https://www.facebook.com/agusdwi89">agus dwi</a></p>
				</div>
			</div>
		</div>
	</div>
</footer>