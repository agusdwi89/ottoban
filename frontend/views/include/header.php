	
<?
	$CI =& get_instance();
	$ticker	= $CI->db->get_where('gs_config',array('cog_id'=>2))->row()->cog_value;

	$main_menu = array(
		array('Home',''),
		array('Dari Redaksi','site/redaksi'),
		array('Artikel','artikel'),
		array('Action Jurnal','jurnal'),
		array('Publikasi Mahasiswa','#'),
		array('Ethical Clearance','#'),
		array('Berita','berita'),
		array('Gallery','gallery'),
		array('About','site/about')
	);

	$jr_kategori 	= $this->db->get_where('jr_kategori',array('status'=>1));
	$jr_prodi 		= $this->db->get_where('jr_prodi',array('status'=>1));

	$mn_active = null;
	if (isset($CI->mn_active)) {
		$mn_active = $CI->mn_active;
	}
?>
<?=$this->load->view('include/msg');?>
<!-- This one in here is responsive menu for tablet and mobiles -->
<div class="responsive-navigation visible-sm visible-xs">
	<a href="#" class="menu-toggle-btn">
		<i class="fa fa-bars"></i>
	</a>
	<div class="responsive_menu">
		<ul class="main_menu">
			<?foreach ($main_menu as $key): ?>
				<li><a href="<?=base_url()?><?=$key[1];?>"><?=$key[0];?></a></li>			
			<?endforeach;?>
		</ul>
	</div> 
</div> 


<header class="site-header">
	<div class="container">
		<div class="row">
			<div class="col-md-4 header-left">
				<!-- <p><i class="fa fa-phone"></i> +01 2334 853</p>
				<p><i class="fa fa-envelope"></i> <a href="mailto:email@universe.com">email@universe.com</a></p> -->
			</div>
			<div class="col-md-4">
				<div class="logo">
					<a href="index.html" title="Universe" rel="home">
						<img src="<?=base_url()?>assets/fe/images/logo.png" alt="Universe">
					</a>
				</div>
			</div>
			<div class="col-md-4 header-right">
				<!-- <ul class="small-links">
					<li><a href="index.html#">About Us</a></li>
					<li><a href="index.html#">Contact</a></li>
					<li><a href="index.html#">Apply Now</a></li>
				</ul>
				<div class="search-form">
					<form name="search_form" method="get" action="#" class="search_form">
						<input type="text" name="s" placeholder="Search the site..." title="Search the site..." class="field_search">
					</form>
				</div> -->
			</div>
		</div>
	</div>
	<div class="nav-bar-main" role="navigation">
		<div class="container">
			<nav class="main-navigation clearfix visible-md visible-lg" role="navigation">
				<ul class="main-menu sf-menu">
					<?foreach ($main_menu as $key): ?>
						<? $mc = (strtolower($key[0]) == strtolower($mn_active))? 'active' : '';?>
						<li class="<?=$mc;?>">
							<?
								$mn_url = ($key[1]=="#")?"#": base_url().$key[1];
							?>
							<a href="<?=$mn_url;?>"><?=$key[0];?></a>
							<?if ($key[0] == "Action Jurnal"): ?>
								<ul>
									<?foreach ($jr_kategori->result() as $key): ?>
										<li><a href="<?=base_url()?>jurnal/index/0/?kategori=<?=$key->id;?>"><?=$key->nama;?></a></li>
									<?endforeach;?>
								</ul>
							<?elseif ($key[0] == "Publikasi Mahasiswa"):?>
								<ul>
									<?foreach ($jr_prodi->result() as $key): ?>
									<li><a href="<?=base_url()?>jurnal/index/0/?prodi=<?=$key->id;?>&mhs="><?=$key->nama;?></a></li>
									<?endforeach;?>
								</ul>
							<?elseif ($key[0] == "Ethical Clearance"):?>
								<ul>
									<li><a href="<?=base_url()?>site/penulis">Penulis</a></li>
									<li><a href="<?=base_url()?>site/judul">Judul</a></li>
									<li><a href="<?=base_url()?>site/tahun">Tahun</a></li>
								</ul>
							<?endif;?>
						</li>
					<?endforeach;?>
				</ul>
			</nav>
		</div>
	</div>
</header>

<div class="container">
	<div style="background:#d5d5d5;padding-top:10px;padding-bottom:10px;padding-left:10px" >
		<marquee behavior="scroll" scrollamount="1" direction="left" style="margin:10px">
			<?=$ticker;?>
		</marquee>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('marquee').marquee('pointer').mouseover(function () {
			$(this).trigger('stop');
		}).mouseout(function () {
			$(this).trigger('start');
		}).mousemove(function (event) {
			if ($(this).data('drag') == true) {
				this.scrollLeft = $(this).data('scrollX') + ($(this).data('x') - event.clientX);
			}
		}).mousedown(function (event) {
			$(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
		}).mouseup(function () {
			$(this).data('drag', false);
		});
	})
</script>