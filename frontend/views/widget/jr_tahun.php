<?
	$wdt_tahun = $this->db->get('v_terbitan');
?>
<div class="widget-item">
	<div class="request-information">
		<h4 class="widget-title">Tahun Terbit Jurnal</h4>
		<div class="widget-inner">
			<ul class="mixitup-controls">
				<?foreach ($wdt_tahun->result() as $key): ?>
					<li class="filter">
						<a href="<?=base_url()?>jurnal/index/0?q=&kategori=&prodi=&tahun=<?=$key->terbitan;?>">Edisi <?=$key->terbitan;?></a>
					</li>
				<?endforeach;?>
			</ul>
		</div>
	</div>
</div>