<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="widget-main">
				<div class="widget-main-title">
					<h4 class="widget-title">Login Anggota</h4>
				</div>
				<div class="widget-inner">
					<div class="request-information">
						<?=form_open('',array('class'=>'request-info clearfix'))?>
							<div class="full-row">
								<label for="yourname">Email:</label>
								<input type="text" id="yourname" name="email">
							</div>
							<div class="full-row">
								<label for="yourname">Password:</label>
								<input type="password" id="yourname" name="password">
							</div>
							<div class="full-row">
								<div style="float:left">
									<i>tidak punya akun ?</i><br> <a href="<?=base_url('user/register')?>"><b>daftar</b></a>
								</div>
								<div class="submit_field">
									<input class="mainBtn pull-right" type="submit" name="" value="login">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("form").validate({
			rules: {
				email		: {required:true,email:true},
				password	: "required"
			}
		});
	})
</script>