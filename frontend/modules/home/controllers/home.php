<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {	

	function __construct(){
		parent::__construct();
		$this->directory = $this->get_current_directory();
	}

	public function index(){
		$this->counting();

		$all_list = [];
		$list = $this->db->get_where('v_section_type_list',array('directory'=>$this->directory));

		foreach ($list->result() as $key) {
			$ls = $this->db->get_where($key->db_table,array('list_id' => $key->id));
			if ($ls->num_rows > -1) {
				$all_list[] = array(
					'layout'	=> $key->db_table,
					'menu_id'	=> $key->menu,
					'data'		=> $ls
					);
			}
		}

		$data['list_section']	=	$all_list;
		$data['directory']		=	$this->db->get_where('_directory',array('directory'=>$this->directory))->row();
		$this->load->view('home_main_view',$data);
	}

	private function get_current_directory(){
		$uri 		= explode("/", $_SERVER['REQUEST_URI']);
		$directory 	= $uri[2];

		if (strlen($directory)== 0) {
			return "_global";
		}else{
			$dbdir = $this->db->get_where('_directory',array('directory'=>$directory));
			if ($dbdir->num_rows() == 1) {
				$dbdir = $dbdir->row();
				if ($dbdir->active == 0) {
					show_404();
				}else return $directory;
			}else{
				show_404();
			}
		}
	}

	private function counting(){
		$this->customer_ip 	= $this->input->ip_address();
		$data['ip'] 		= $this->customer_ip;
		$this->db->insert('visitor',$data);
	}
}