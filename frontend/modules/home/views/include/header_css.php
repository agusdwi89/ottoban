<!-- Favicon and Touch Icons -->
<link href="<?=base_url()?>assets/fe/images/favicon/favicon.png" rel="shortcut icon" type="image/png">
<link href="<?=base_url()?>assets/fe/images/favicon/apple-touch-icon.png" rel="apple-touch-icon">
<link href="<?=base_url()?>assets/fe/images/favicon/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="<?=base_url()?>assets/fe/images/favicon/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="<?=base_url()?>assets/fe/images/favicon/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Icon fonts -->
<link href="<?=base_url()?>assets/fe/css/font-awesome.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/fe/css/flaticon.css" rel="stylesheet">

<!-- Bootstrap core CSS -->
<link href="<?=base_url()?>assets/fe/css/bootstrap.min.css" rel="stylesheet">

<!-- Plugins for this template -->
<link href="<?=base_url()?>assets/fe/css/animate.css" rel="stylesheet">
<link href="<?=base_url()?>assets/fe/css/owl.carousel.css" rel="stylesheet">
<link href="<?=base_url()?>assets/fe/css/owl.theme.css" rel="stylesheet">
<link href="<?=base_url()?>assets/fe/css/slick.css" rel="stylesheet">
<link href="<?=base_url()?>assets/fe/css/slick-theme.css" rel="stylesheet">
<link href="<?=base_url()?>assets/fe/css/owl.transitions.css" rel="stylesheet">
<link href="<?=base_url()?>assets/fe/css/jquery.fancybox.css" rel="stylesheet">
<link href="<?=base_url()?>assets/fe/css/magnific-popup.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<?=base_url()?>assets/fe/css/style.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
