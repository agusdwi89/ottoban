<footer class="site-footer">
	<div class="container">
		<div class="row">
			<div class="col col-sm-2 footer-logo-wrapper">
				<img src="<?=base_url()?>assets/fe/images/footer-logo.png" alt class="img img-responsive">
			</div>

			<div class="col col-sm-7 copyright-info-wrapper">
				<p class="copyright-info">&copy; 2017 Galaxy. Is Powered By <a href="#">ThemeJangle</a></p>
			</div>

			<div class="col col-sm-3 social-links-wrapper">
				<ul class="social-links">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
					<li><a href="#"><i class="fa fa-behance"></i></a></li>
					<li><a href="#"><i class="fa fa-youtube"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>