<?$data = $data->row();?>
<section class="agency-business-solution-skill section-padding" id="about">
	<div class="container">
		<div class="row">
			<div class="col col-lg-6 skills-col">
				<div class="section-title">
					<h2><?=$data->title;?></h2>
				</div>
				<?=$data->description;?>
			</div>
			<div class="col col-lg-6 images-holder-col-wrapper">
				<div class="images-holder-col">
					<div class="img-holder">
						<img src="<?=base_url()?>media/images/<?=$data->image_1;?>" alt class="img img-responsive">
					</div>
					<div class="img-holder">
						<img src="<?=base_url()?>media/images/<?=$data->image_2;?>" alt class="img img-responsive">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>