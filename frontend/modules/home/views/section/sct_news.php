<?
	$data 	= $data->row();
	$items 	= $this->db->order_by('id','desc')->limit(6)->get_where('sct_news_items',array('sct_news_id'=>$data->id)); 
?>

<section class="agency-blog section-padding" id="news">
	<div class="container">
		<div class="row section-title">
			<div class="col col-md-8 col-md-offset-2">
				<h2><?=$data->title;?></h2>
				<p><?=$data->description;?></p>
			</div>
		</div>
		<div class="row">
			<div class="col col-xs-12">
				<div class="agency-blog-slider agency-blog-grids dots-style1">
					<?foreach ($items->result() as $news): ?>
						<div class="grid">
							<div class="entry-header">
								<img src="<?=base_url()?>media/news/<?=$news->image;?>" alt>
							</div>
							<div class="entry-details">
								<span class="entry-date"><?=pretty_date($news->date,false);?></span>
								<h3><a href="#"><?=$news->title;?></a></h3>
								<p><?=smart_trim($news->content,100);?></p>
								<div class="entry-footer">
									<a href="<?=base_url()?>read/<?=$news->id;?>" class="more">Read more</a>
								</div>
							</div>
						</div>
					<?endforeach;?>
				</div>
			</div>
		</div>
	</div>
</section>