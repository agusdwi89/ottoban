<section class="agency-services section-padding" id="services">
	<div class="container">
		<div class="row agency-services-grids">
			<?foreach ($data->result() as $ic): ?>
				<div class="col col-md-4">
					<div class="grid">
						<div class="icon">
							<img src="<?=base_url()?>media/icon/<?=$ic->icon;?>" class="animated-svg-icon-s4">
						</div>
						<h3><?=$ic->title;?></h3>
						<p><?=$ic->description;?></p>
					</div>
				</div>
			<?endforeach;?>
		</div>
	</div>
</section>