<?
	$data 	= $data->row();
	$typed 	= json_decode($data->typed_string);
?>

<script type="text/javascript">
	var TYPED_STRING = <?=$data->typed_string;?>;
</script>

<section class="hero hero-s2 parallax" data-bg-image="<?=base_url()?>media/showcase/<?=$data->background_image;?>" data-speed="3">
    <div class="container">
        <div class="row">
            <div class="col col-md-9 hero-info">
                <h4 class="sub-title"><?=$data->title;?></h4>
                <h1 class="title"><?=$data->sub_title_1;?><span><?=$data->sub_title_2;?></span><?=$data->sub_title_3;?><span class="agency-typed-element"></span></h1>
                
                <?if (strlen($data->button_text) > 0): ?>
                	<a href="<?=($data->button_type == "external" ? "" : "#" );?><?=$data->button_link;?>" target="<?=($data->button_type == "external" ? "_blank" : "" );?>" class="agency-btn-s1 generated_button"><?=$data->button_text;?><span><i class="fa fa-angle-down"></i></span></a>	
                <?endif;?>

            </div>
        </div>
    </div>
</section>