<?
	$data 	= $data->row();
	$items 	= $this->db->get_where('sct_testimonial_items',array('sct_testimonial_id'=>$data->id));
?>
<section class="agency-testimonials section-padding" id="testimonials">
	<div class="container">
		<div class="row section-title">
			<div class="col col-md-8 col-md-offset-2">
				<h2><?=$data->title;?></h2>
				<p><?=$data->description;?></p>
			</div>
		</div> 
		<div class="row">
			<div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
				<div class="agency-testimonial-slider dots-style1">
					<?foreach ($items->result() as $p): ?>
						<div class="box">
							<div class="client-pic">
								<img style="width:100px;height:100px" src="<?=base_url()?>media/images/<?=$p->photo;?>" alt>
							</div>
							<div class="client-quote">
								<div class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
								</div>
								<p>“<?=$p->testi;?>”</p>
							</div>
							<div class="client-info">
								<h5><?=$p->name;?></h5>
								<span><?=$p->company;?></span>
							</div>
						</div>
					<?endforeach;?>
				</div>
			</div>
		</div> 
	</div> 
</section>