<header class="site-header header-style-2" id="<?=$menu_id;?>">
	<nav class="navigation navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="open-btn">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><img src="<?=base_url()?>assets/fe/images/logo-agency.png" alt></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse navbar-right navigation-holder">
				<button class="close-navbar"><i class="fa fa-close"></i></button>
				<ul class="nav navbar-nav">
					<?foreach ($data->result() as $menu): ?>
						<li class="<?=($menu->url == 'home' ? 'current-menu-item' : '');?>"><a href="#<?=$menu->url;?>"><?=$menu->name;?></a></li>	
					<?endforeach;?>
				</ul>
			</div>
			<div class="side-menu">
				<button class="btn side-menu-open-btn"><i class="fi flaticon-menu"></i></button>
				<div class="side-menu-inner">
					<button class="btn side-menu-close-btn"><i class="fa fa-times"></i></button>
					<div class="logo">
						<img src="<?=base_url()?>assets/fe/images/logo.png" alt>
					</div>
				</div>
			</div>
		</div>
	</nav>
</header>