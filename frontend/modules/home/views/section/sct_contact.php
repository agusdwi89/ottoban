<?$data = $data->row();?>
<section class="agency-contact-section" id="contact">
	<div class="container contact-block">
		<div class="row">
			<div class="col col-xs-12">
				<h2><?=$data->title;?></h2>
				<div class="contact-info">
					<p><?=$data->description;?></p>
					<ul>
						<li>
							<span class="icon">
								<i class="fi flaticon-web"></i>
							</span>
							<?=$data->address;?>
							<div class="view-map">
								<a href="<?=$data->maps_link;?>" class="view-map-btn map-link"><img src="<?=base_url()?>assets/fe/images/view-map.png" alt>View on map</a>
							</div>
						</li>
						<li>
							<span class="icon">
								<i class="fi flaticon-technology"></i>
							</span>
							<?=$data->telp;?>
						</li>
						<li>
							<span class="icon">
								<i class="fi flaticon-mail"></i>
							</span>
							<?=$data->email;?>
						</li>
					</ul>
				</div>
				<div class="contact-form">
					<form class="form" id="contact-form">
						<div>
							<input type="text" name="name" class="form-control" placeholder="Full Name">
						</div>
						<div>
							<input type="email" name="email" class="form-control" placeholder="Email">
						</div>
						<div>
							<select class="form-control" name="topic">
								<option selected disabled>Select topic</option>
								<option>Topic 1</option>
								<option>Topic 2</option>
								<option>Topic 3</option>
							</select>
						</div>
						<div>
							<textarea class="form-control" name="message" placeholder="write"></textarea>
						</div>
						<div class="submit-btn">
							<div class="btn-wrapper">
								<button type="submit" class="app-btn-s1">Submit</button>
								<span id="loader"><img src="<?=base_url()?>assets/fe/images/contact-ajax-loader.gif" alt="Loader"></span>
							</div>
						</div>
						<div class="col col-md-12 error-handling-messages">
							<div id="success">Thank you</div>
							<div id="error"> Error occurred while sending email. Please try again later. </div>
						</div>
					</form>
				</div>

				<p class="privacy"><?=$data->bottom_text;?></p>
			</div>
		</div>
	</div>
</section>