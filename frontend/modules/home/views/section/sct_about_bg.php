<?$data = $data->row();?>
<section class="agency-cta section-padding parallax" data-bg-image="<?=base_url()?>media/images/cta-bg.jpg" data-speed="8">
	<div class="container">
		<div class="row">
			<div class="col col-md-6 col-md-offset-3">
				<h2><?=$data->title;?></h2>
				<p><?=$data->description;?></p>
				<?if (strlen($data->button_text) > 0): ?>
                	<a href="<?=($data->button_type == "external" ? "" : "#" );?><?=$data->button_link;?>" target="<?=($data->button_type == "external" ? "_blank" : "" );?>" class="agency-btn-s2 generated_button"><?=$data->button_text;?></a>
                <?endif;?>
			</div>
		</div>
	</div>
</section>