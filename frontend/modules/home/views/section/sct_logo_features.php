<section class="agency-latest-features">
	<div class="container">
		<div class="row">
			<div class="col col-xs-12 grids">
				<?foreach ($data->result() as $img): ?>
					<div class="grid">
						<img src="<?=base_url()?>media/logo/<?=$img->image;?>" alt="">
					</div>	
				<?endforeach;?>
			</div>
		</div>
	</div>
</section>