<section class="agency-funfact section-padding">
	<div class="container start-count">    
		<div class="row content">
			<?foreach ($data->result() as $fun): ?>
				<div class="col col-sm-3 col-xs-6">
					<div class="box">
						<div class="icon">
							<img src="<?=base_url()?>media/icon/<?=$fun->icon;?>" alt>
						</div>
						<?if ($fun->number > 0): ?>
							<h3><span class="counter" data-count="<?=$fun->number;?>">00</span></h3>
						<?endif;?>
						<p><?=$fun->text;?></p>
					</div>
				</div>
			<?endforeach;?>
		</div>
	</div>
</section>