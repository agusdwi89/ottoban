<?
	$data 		= $data->row();
	$products	= $this->db->get_where('sct_product_items',array('sct_product_id'=>$data->id)); 
?>
<section class="agency-projects section-padding">
	<div class="container">
		<div class="row section-title">
			<div class="col col-md-8 col-md-offset-2">
				<h2><?=$data->title;?></h2>
				<p><?=$data->description;?></p>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col col-xs-12">
				<div class="agnecy-projects-slider agency-project-grids">
					<?foreach ($products->result() as $p): ?>
						<div class="grid">
							<div class="img-holder">
								<img src="<?=base_url()?>media/products/<?=$p->image;?>" alt>
							</div>
							<div class="details">
								<h3><?=$p->title;?></h3>
								<p><?=$p->price;?></p>
							</div>
						</div>
					<?endforeach;?>
				</div>
			</div>
		</div>
	</div>
</section>