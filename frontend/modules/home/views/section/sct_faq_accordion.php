<?
	$data = $data->row();
	$faq  = json_decode($data->faq);
?>
<style type="text/css">
	.agency-faq:before{
		background: url("<?=base_url()?>media/images/<?=$data->image;?>") left center/cover no-repeat local !important;
	}
</style>
<section class="agency-faq">
	<div class="container">
		<div class="row">
			<div class="col col-lg-6 left-col">
				<div class="section-title">
					<h2><?=$data->title;?></h2>
				</div>
				<div class="panel-group" id="accordion">
					<?$i=0;foreach ($faq as $f): $i++;?>
						<div class="panel panel-default">
							<div class="panel-heading">
								<a <?=(($i>1) ? 'class="collapsed"':'')?> data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i;?>" <?=(($i==1) ? 'aria-expanded="true"':'')?>><span>0<?=$i;?>.</span> <?=$f->title;?></a>
							</div>
							<div id="collapse<?=$i;?>" class="panel-collapse collapse <?=(($i==1) ? 'in':'')?>">
								<div class="panel-body">
									<p><?=$f->description;?></p>
								</div>
							</div>
						</div>
					<?endforeach;?>
				</div>
			</div>
			<div class="col col-lg-6 right-col"></div>
		</div>
	</div>
</section>