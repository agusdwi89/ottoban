<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="themexriver">
    <title> <?=$directory->meta_title;?></title>
    <meta name="description" content="<?=$directory->meta_description;?>">
    <meta name="keywords" content="<?=$directory->meta_keywords;?>">
    <?=$this->load->view('include/header_css');?>

</head>

<body id="home" class="home-agency home-with-side-menu">
    <div class="page-wrapper">
        <!-- <div class="preloader"><div class="loader-inner"><span></span><span></span><span></span><span></span></div></div> -->

        <?php foreach ($list_section as $key): ?>
            <?//debug_array($key);?>  
            <?=$this->load->view('section/'.$key['layout'],$key);?>
        <?php endforeach ?>

    </div>

    <?=$this->load->view('include/footer_js');?>
    
</body>
</html>