<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		if (is_login()) {redirect(base_url('manage/dashboard'));}

		if (is_post()) {
			if(login_cek($this->input->post('username'),$this->input->post('password'))){
				
				$this->session->set_flashdata('message','selamat datang :D');
				redirect(base_url('manage/dashboard'));
			}else{
				$this->session->set_flashdata('message','maaf username / password salah');
				redirect(base_url('manage/login'));
			}
		} 
		$this->load->view('login_page');
	}

	function off(){
		logout();redirect(base_url('manage/login'));
	}

	function set_default_subdomain(){
		$this->session->set_userdata('session_subdomain', 'all');
		echo "{set-subdomain-all}";
	}
}