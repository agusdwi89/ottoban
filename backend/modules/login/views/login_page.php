<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript">
        var BASE = "<?=base_url()?>";
        var econ = [];
    </script>

    <?=$this->load->view('include/header_css');?> 
    <?=$this->load->view('include/footer_script');?>
    <?=$this->load->view('include/msg');?>

</head>
<body>
    <?=form_open('/',array('id'=>'global-form'))?><?=form_close()?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="wrapper-page">
                        <div class="m-t-40 account-pages">
                            <div class="text-center account-logo-box">
                                <h2 class="text-uppercase">
                                    <a href="index.html" class="text-success">
                                        <span><img src="<?=base_url()?>assets/be/images/logo_dark.png" alt="" height="30"></span>
                                    </a>
                                </h2>
                            </div>
                            <div class="account-content">
                                <?=form_open('',array('class'=>'form-horizontal'))?>
                                    <div class="form-group m-b-25">
                                        <div class="col-12">
                                            <label for="emailaddress">Username</label>
                                            <input name="username" class="form-control input-lg" type="text" id="emailaddress" required="" placeholder="your username">
                                        </div>
                                    </div>
                                    <div class="form-group m-b-25">
                                        <div class="col-12">
                                            <label for="password">Password</label>
                                            <input name="password" class="form-control input-lg" type="password" required="" id="password" placeholder="Enter your password">
                                        </div>
                                    </div>
                                    <div class="form-group account-btn text-center m-t-10">
                                        <div class="col-12">
                                            <button class="btn w-lg btn-rounded btn-lg btn-primary waves-effect waves-light" type="submit">Sign In</button>
                                        </div>
                                    </div>
                                <?=form_close()?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</html>