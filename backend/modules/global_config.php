<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Global_config extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "global_config";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	public function index($tab = "social")
	{
		$this->tab = $tab;

		if (is_post()) {
			$data = $this->input->post();
			foreach ($data as $key => $value) {
				$data = array('value' => $value );
				$this->db->where('name', $key);
				$this->db->update('global_config', $data);
			}
			$this->session->set_flashdata('message','Data Saved Successfully');	
			redirect(base_url('manage/global_config/'));
		}

		$data['data'] = array(
			"instagram"	=> $this->db->get_where('global_config',array('name'=>'instagram'))->row()->value,
			"facebook"	=> $this->db->get_where('global_config',array('name'=>'facebook'))->row()->value,
			"twitter"	=> $this->db->get_where('global_config',array('name'=>'twitter'))->row()->value,
			"mail"		=> $this->db->get_where('global_config',array('name'=>'mail'))->row()->value,
			"phone"		=> $this->db->get_where('global_config',array('name'=>'phone'))->row()->value
		);

		$data['value_pure'] 	= $this->db->get_where('global_config',array('name'=>'pure-chat'))->row()->value;
		$data['other_prod'] 	= $this->db->get('other_products');
		$data['local_view'] 	= 'v_global_config';
		$this->load->view('v_manage',$data);
	}

	function set_pure(){
		$item = $this->input->post();

		$this->db->where('name', $item['name']);
		$this->db->update('global_config', array('value'=>$item['value'])); 
		
		$this->session->set_flashdata('message','Data Saved Successfully');	
		redirect(base_url('manage/global_config/index/pure'));
	}

	function set_logo(){
		$this->upload('logo.png');
		$this->session->set_flashdata('message','Data Saved Successfully');	
		redirect(base_url('manage/global_config/index/logo'));
	}

	function set_icon(){
		$this->upload('favicon.ico');
		$this->session->set_flashdata('message','Data Saved Successfully');	
		redirect(base_url('manage/global_config/index/logo'));
	}

	function upload($name){
		$_FILES['userfile']['name']		= strtolower($_FILES['userfile']['name']);
		$config['upload_path']			= 'assets/geniot/images/';
		$config['allowed_types']		= '*';
		$config['max_size']				= '10000';
		$config['max_width']			= '800';
		$config['max_height']			= '1024';
		$config['overwrite'] 			= TRUE;
		$config['file_name'] 			= $name;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload()){
			return array(false,$this->upload->display_errors());
		}else{
			$a = $this->upload->data();
			return array(true,$a);
		}
	}

	function set_other_prod(){
		$data = $this->input->post();
		$this->db->insert('other_products',$data);
		$this->session->set_flashdata('message','Data Saved Successfully');	
		redirect(base_url('manage/global_config/index/other_prod'));
	}

	function delete_other($id){
		$this->db->delete('other_products', array('id' => $id)); 
		$this->session->set_flashdata('message','Data Deleted Successfully');	
		redirect(base_url('manage/global_config/index/other_prod'));
	}
}