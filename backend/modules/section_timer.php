<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Section_timer extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "section";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	function edit($id){
		if (is_post()) {
			
			$item = $this->input->post('ar');

			if ($_FILES['image']['size'] > 0 ){
				$upload = $this->upload('image');
				if ($upload[0])
					$item['image'] = $upload[1]['file_name'];
			}

			$this->db->where('id', $id);
			$this->db->update('section_timer', $item); 
			$this->session->set_flashdata('message','Data Saved Successfully');
			redirect(base_url("manage/section_timer/edit/$id"));
		}
		$data['items'] 		= $this->db->get_where('section_timer',array('id'=>$id))->row();
		$data['local_view'] = 'v_section_timer';
		$this->load->view('v_manage',$data);
	}

	function upload($name){
		$_FILES['userfile']['name']	= strtolower($_FILES['userfile']['name']);
		$config['upload_path']		= 'assets/section';
		$config['allowed_types']	= 'jpg|png';
		$config['max_size']			= '10000';
		$config['max_width']		= '5000';
		$config['max_height']		= '5000';
		$config['encrypt_name']		= true;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload($name)){
			return array(false,$this->upload->display_errors());
		}else{
			$a = $this->upload->data();
			return array(true,$a);
		}
	}

}