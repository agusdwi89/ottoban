<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "dashboard";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}


	public function index()
	{
		$data['local_view'] = 'v_dashboard';
		$this->load->view('v_manage',$data);
	}

	function load($date=""){
		echo "hello";
		// $this->cdate = ($date == "")? "" : explode("s", $date);

		// $data['topstat']['visit_today'] 	= $this->get_visitor_today();
		// $data['topstat']['user_total'] 		= $this->get_total_user();
		// $data['topstat']['sales_real'] 		= $this->get_total_sales();
		// $data['topstat']['sales_potency'] 	= $this->get_total_sales(' <> "completed" and order_status <> "refund" and order_status <> "cancel" ');

		// $data['topstat']['sales_cancel'] 	= $this->get_total_sales(' = "cancel"');
		// $data['topstat']['sales_refund'] 	= $this->get_total_sales(' = "refund"');
		// $data['topstat']['user_cancel'] 	= $this->get_total_revcancel('cancel');
		// $data['topstat']['user_refund'] 	= $this->get_total_revcancel('refund');
		
		// $data['top10']['user'] 	= $this->last10_user();
		// $data['top10']['order'] = $this->last10_order();

		// $this->load->view('manage/v_dashboard_load',$data);
	}

	function get_total_revcancel($condition){
		$date = date("Y-m-d");
		
		if ($this->sub_domain == "all") 
			$sql = " select count(id) as total from orders where {$this->qbu('time_order')} and `order_status` = '$condition'";
		else $sql = "select count(id) as total from orders where subdomain = '{$this->sub_domain}' and {$this->qbu('time_order')} and `order_status` = '$condition'";

		$sql = $this->db->query($sql)->row();
		return $sql->total;
	}

	function get_visitor_today(){
		$date = date("Y-m-d");
		
		if ($this->sub_domain == "all") 
			$sql = "select COUNT(DISTINCT ip) as v  FROM visitor where {$this->qbu('date')}";
		else $sql = "select COUNT(DISTINCT ip) as v  FROM visitor where subdomain = '{$this->sub_domain}' and {$this->qbu('date')}";

		$sql = $this->db->query($sql);
		return ($sql->num_rows() == 1)? $sql->row()->v : "0" ;
	}

	function qbu($i){
		return "$i >= '{$this->cdate[0]}' and $i <= DATE_ADD('{$this->cdate[1]}', INTERVAL 1 DAY)";
	}

	function get_total_user(){

		if ($this->sub_domain == "all") 
			$sql = "select COUNT(DISTINCT cust_email) as total from orders where {$this->qbu('time_order')}";
		else $sql = "select COUNT(DISTINCT cust_email) as total from orders where subdomain = '{$this->sub_domain}' and {$this->qbu('time_order')}";

		$sql = $this->db->query($sql);
		return ($sql->num_rows() == 1)? $sql->row()->total : "0" ;
	}

	function get_visitor_detail(){
		$sql = 'select DATE_FORMAT(date, "%Y-%m-%d") as d,COUNT(DISTINCT ip)  FROM visitor GROUP BY d';
	}

	function get_total_sales($condition = "= 'completed'"){

		if ($this->sub_domain == "all") 
			$sql = "select SUM(prd_total) as total from orders WHERE order_status $condition and {$this->qbu('time_order')}";
		else $sql = "select SUM(prd_total) as total from orders WHERE subdomain = '{$this->sub_domain}' and order_status $condition and {$this->qbu('time_order')}";

		$sql = $this->db->query($sql);
		return ($sql->num_rows() == 1)? $sql->row()->total : "0" ;
	}

	function last10_order(){
		if ($this->sub_domain == "all") 
			$sql = "select * from v_manage_order where {$this->qbu('time_order')} limit 10";
		else $sql = "select * from v_manage_order where subdomain = '{$this->sub_domain}' and {$this->qbu('time_order')} limit 10";

		return $this->db->query($sql);
	}

	function last10_user(){
		if ($this->sub_domain == "all") 
			$top10id = "select * from v_manage_order where id in (select MAX(id) from v_manage_order where {$this->qbu('time_order')} GROUP BY cust_email ORDER BY MAX(id) desc) limit 10";
		else $top10id = "select * from v_manage_order where id in (select MAX(id) from v_manage_order where subdomain = '{$this->sub_domain}' and {$this->qbu('time_order')}  GROUP BY cust_email ORDER BY MAX(id) desc) limit 10";

		$sql = $this->db->query($top10id);
		return $sql;
	}

	function set_sb(){
		$referer = $_SERVER['HTTP_REFERER'];
		$this->session->set_userdata('session_subdomain', $this->input->post('select-subdomain'));
		redirect($referer);
	}

	function backup_db(){
		$q = "mysqldump nexbay > nexbay.sql";
		$this->db->query($q);
	}
}