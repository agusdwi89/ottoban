<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Topmenu extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "menu";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	public function index()
	{
		if (is_post()) {
			$input = $this->input->post();
			$input['value2'] = (isset($input['value2']))? $input['value2'] : "off";
			$this->db->where('name', 'buy-now');
			$this->db->where('subdomain', $this->sub_domain);
			$this->db->update('site_config', $input); 
			$this->session->set_flashdata('message','Data saved successfully');
			redirect(base_url('manage/topmenu'));
		}
        
        if ($this->sub_domain != "all") {
        	$data['section']   	= $this->db->get_where('v_section_name',array('subdomain'=>$this->sub_domain));
        	$data['db']         = $this->db->get_where("top_menu",array('subdomain'=>$this->sub_domain));
        	$data['topbuy']   	= $this->db->get_where('site_config',array('name'=>'buy-now', 'subdomain' => $this->sub_domain))->row();
        }
		$data['local_view'] = 'v_topmenu';
		$this->load->view('v_manage',$data);
	}

	function add(){
		if (is_post()) {
			$input = $this->input->post('def');
			$input['url'] = $this->input->post($input['link']);
			$input['subdomain'] = $this->sub_domain;
			$this->db->insert('top_menu',$input);
			$this->session->set_flashdata('message','Data saved successfully');
			redirect(base_url('manage/topmenu'));
		} 
		
		$data['local_view'] = 'v_topmenu_add';
        $data['section']   	= $this->db->get_where('v_section_name',array('subdomain'=>$this->sub_domain));
		$this->load->view('v_manage',$data);
	}

	function edit($id){
		if (is_post()) {
			$input = $this->input->post('def');
			$input['url'] = $this->input->post($input['link']);
			$this->db->where('id', $id);
			$this->db->update('top_menu', $input); 
			$this->session->set_flashdata('message','Data saved successfully');
			redirect(base_url('manage/topmenu'));	
		} 
		
		$data['local_view'] = 'v_topmenu_edit';
		$data['db'] 		= $this->db->get_where('top_menu',array('id'=>$id))->row();
		$data['section']   	= $this->db->get_where('v_section_name',array('subdomain'=>$this->sub_domain));
		$this->load->view('v_manage',$data);	
	}

	function delete($id){
		$a = $this->db->delete('top_menu', array('id'=> $id)); 
		$this->session->set_flashdata('message','Data delete successfully');
		redirect(base_url('manage/topmenu'));
	}
}