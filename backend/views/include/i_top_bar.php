<script type="text/javascript">
	$(function(){
		$('.selectpicker').on('change', function(e){
			$("#frm_subdomain").submit();
		});
	})
</script>
<div class="topbar">
	<div class="topbar-left">
		<a href="<?=base_url()?>manage/dashboard" class="logo">
			<span>
				<img src="<?=base_url()?>assets/be/images/logo.png" alt="" height="20">
			</span>
			<i>
				<img src="<?=base_url()?>assets/be/images/logo_sm.png" alt="" height="28">
			</i>
		</a>
	</div>
	<div class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
			<ul class="nav navbar-left">
				<li>
					<button type="button" class="button-menu-mobile open-left waves-effect">
						<i class="dripicons-menu"></i>
					</button>
				</li>
				<li class="d-none d-sm-block lang-option">
					<a href="http://<?=$_SERVER['HTTP_HOST']?>" target="_blank" style="    margin-top: 13px;margin-left: -15px;">
						<span class="badge badge-purple">view site</span>
					</a>
				</li>
			</ul>
			<ul class="nav navbar-right list-inline">
				<li class="list-inline-item">
					<div class="dropdown">
						<a title="logout" href="<?=base_url('manage/login/off')?>" class="right-menu-item dropdown-toggle dz-tip">
							<i class="dripicons-power"></i>
						</a>
					</div>
				</li>
				<!-- <li class="dropdown user-box list-inline-item">
					<a href="" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
						<img src="<?=base_url()?>assets/images/users/avatar-1.jpg" alt="user-img" class="rounded-circle user-img">
					</a>
					<ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
						<li><a href="javascript:void(0)" class="dropdown-item">Change Password</a></li>
						<li><a href="<?=base_url('manage/login/off')?>" class="dropdown-item">Logout</a></li>
					</ul>
				</li> -->
			</ul> 
		</div>
	</div>
</div>