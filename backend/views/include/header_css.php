<meta charset="utf-8" />
<title>Ottoban :: Management Page</title>

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
<meta content="Coderthemes" name="author" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<!-- App favicon -->
<link rel="shortcut icon" href="<?=base_url()?>assets/be/images/favicon.ico">

<!-- C3 charts css -->
<link href="<?=base_url()?>assets/be/plugins/c3/c3.min.css" rel="stylesheet" type="text/css"  />

<!-- App css -->
<link href="<?=base_url()?>assets/be/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/be/css/metismenu.min.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/be/css/icons.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/be/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>assets/be/css/simple-line-icons.css" rel="stylesheet" type="text/css" />

<link href="<?=base_url()?>assets/be/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />

<script src="<?=base_url()?>assets/be/js/modernizr.min.js"></script>