<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Menu_nest {
	
	private $menu = array();
	
    function built_tree($data, $parent = 0) {
    	static $i = 1;
    	$c = '';
    	$tab = str_repeat("\t\t", $i);
    	if (isset($data[$parent])) {
    		$html 	= "\n$tab<ol class='dd-list'>";
    		$i++;
    		foreach ($data[$parent] as $v) {
    			$child = $this->built_tree($data, $v->menu_id);
    			if ($child) {
    				$c = "hasul";
    			}else $c='';
    			$html .= "\n\t$tab<li class='dd-item' data-id='{$v->menu_id}'>";
    			$html .= '<div class="dd-handle"></div><a class="hh" href="#">' . $v->menu_nama . '<span uid="'.$v->menu_id.'" class="deletemenu">X</span></a><div class="clearfix"></div>';
    			if ($child) {
    				$i--;
    				$html .= $child;
    				$html .= "\n\t$tab";
    			}
    			$html .= '</li>';
    		}
    		$html .= "\n$tab</ol>";
    		return $html;
    	} else {
    		return false;
    	}
    }
	
	function get()
	{
		$CI =& get_instance();
		$CI->db->order_by('menu_sort,parent_id');
		$ds = $CI->db->get('gs_menu');
		$data = array();
		foreach ($ds->result() as $row) {
			$data[$row->parent_id][] = $row;
		}
		return $this->built_tree($data);
	}
}