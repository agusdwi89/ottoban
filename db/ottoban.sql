/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : ottoban

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 16/05/2019 13:28:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for _directory
-- ----------------------------
DROP TABLE IF EXISTS `_directory`;
CREATE TABLE `_directory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory` varchar(100) DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  `online` enum('yes','no') DEFAULT 'yes',
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `meta_keywords` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of _directory
-- ----------------------------
BEGIN;
INSERT INTO `_directory` VALUES (1, '_global', 1, 'yes', 'global title', 'global description', 'global keyword');
COMMIT;

-- ----------------------------
-- Table structure for global_config
-- ----------------------------
DROP TABLE IF EXISTS `global_config`;
CREATE TABLE `global_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of global_config
-- ----------------------------
BEGIN;
INSERT INTO `global_config` VALUES (1, 'instagram', 'https://www.instagram.com/nexbaystore/');
INSERT INTO `global_config` VALUES (2, 'facebook', 'https://www.facebook.com/nexbaystore/');
INSERT INTO `global_config` VALUES (3, 'twitter', 'https://twitter.com/nexbaystore');
INSERT INTO `global_config` VALUES (4, 'pure-chat', '<script type=\'text/javascript\' data-cfasync=\'false\'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement(\'script\'); script.async = true; script.type = \'text/javascript\'; script.src = \'https://app.purechat.com/VisitorWidget/WidgetScript\'; document.getElementsByTagName(\'HEAD\').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == \'loaded\' || this.readyState == \'complete\')) { var w = new PCWidget({c: \'287127aa-18d5-40bf-a114-848bd8c680f9\', f: true }); done = true; } }; })();</script>\n\n\n<!--\n<link href=\"https://cdn.prooffactor.com/javascript/dist/1.0/style.css\" rel=\"stylesheet\"><div id=\"juicier-container\" data-account-id=\"27sA2la6xfOak48I38M1lIfg88d2\"><a href=\"https://prooffactor.com\" target=\"_blank\">Powered by ProofFactor - Social Proof Notifications</a></div><script type=\"text/javascript\" src=\"https://cdn.prooffactor.com/javascript/dist/1.0/jcr-widget.js\"></script>\n-->');
INSERT INTO `global_config` VALUES (5, 'copyright', '&copy; Copyright 2019 Nexbay Developed by <a target=\"_blank\" href=\"https://iqonicthemes.com/\">MARCCO Global</a>.');
INSERT INTO `global_config` VALUES (6, 'mail', 'info@nexbay.id');
INSERT INTO `global_config` VALUES (7, 'phone', '+6281585023190');
COMMIT;

-- ----------------------------
-- Table structure for gs_users
-- ----------------------------
DROP TABLE IF EXISTS `gs_users`;
CREATE TABLE `gs_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `jenis` enum('superadmin','admin') NOT NULL DEFAULT 'admin',
  `status` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gs_users
-- ----------------------------
BEGIN;
INSERT INTO `gs_users` VALUES (1, 'info@ottoban.com', 'ottoadmin', '3a6b3c8fba20d7b709bc49b0a921e018', 'admin', 1);
COMMIT;

-- ----------------------------
-- Table structure for sct_about
-- ----------------------------
DROP TABLE IF EXISTS `sct_about`;
CREATE TABLE `sct_about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image_1` varchar(255) DEFAULT NULL,
  `image_2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_about
-- ----------------------------
BEGIN;
INSERT INTO `sct_about` VALUES (1, 4, 'We Give Solution of Business', '<p>Lorem Ipsum is simply dummy text of the printing and ypesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s. world\'s biggest collection.</p>', 'img-1.jpg', 'img-2.jpg');
COMMIT;

-- ----------------------------
-- Table structure for sct_about_bg
-- ----------------------------
DROP TABLE IF EXISTS `sct_about_bg`;
CREATE TABLE `sct_about_bg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `background` varchar(255) DEFAULT NULL,
  `button_type` enum('internal','external') DEFAULT NULL,
  `button_link` varchar(255) DEFAULT NULL,
  `button_text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_about_bg
-- ----------------------------
BEGIN;
INSERT INTO `sct_about_bg` VALUES (1, 6, 'Galaxy is right choice for your business.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy.', 'cta-bg.jpg', 'internal', 'contact', 'Contact Us');
COMMIT;

-- ----------------------------
-- Table structure for sct_contact
-- ----------------------------
DROP TABLE IF EXISTS `sct_contact`;
CREATE TABLE `sct_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `address` varchar(255) DEFAULT NULL,
  `maps_link` varchar(255) DEFAULT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `bottom_text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_contact
-- ----------------------------
BEGIN;
INSERT INTO `sct_contact` VALUES (1, 12, 'Contact our support guys or make appointment with our consultants', 'Please contact us using the information below. For additional information on our management consulting services, please visit the appropriate page on our site.', '131 Dartmouth Street <br> Boston, Massachusetts 02116 <br> United States', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3677.7898246149844!2d89.5601340147084!3d22.810250485060976!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ff901d89110f01%3A0x8dbefa2e360efc60!2z4Kaw4Kef4KeH4KayIOCmruCni-CnnA!5e0!3m2!1sbn!2sbd!4', '+1 617 572 2000', 'support@consult.com', '** We don\'t share your personal info with anyone. Check out our <a href=\"#\">Privacy Policy</a> for more information.');
COMMIT;

-- ----------------------------
-- Table structure for sct_faq_accordion
-- ----------------------------
DROP TABLE IF EXISTS `sct_faq_accordion`;
CREATE TABLE `sct_faq_accordion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `faq` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_faq_accordion
-- ----------------------------
BEGIN;
INSERT INTO `sct_faq_accordion` VALUES (1, 9, 'The simple way to start up Your new Business! ', 'faq-bgaa.jpg', '[{\"title\":\"We Are The Right Choice Of Your Judgement!\",\"description\":\"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blinds  and Separated they live in Bookmarksgrove right \"},{\"title\":\"We Are The Right Choice Of Your Judgement!\",\"description\":\"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blinds  and Separated they live in Bookmarksgrove right \"},{\"title\":\"We Are The Right Choice Of Your Judgement!\",\"description\":\"Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blinds  and Separated they live in Bookmarksgrove right \"}]');
COMMIT;

-- ----------------------------
-- Table structure for sct_footer
-- ----------------------------
DROP TABLE IF EXISTS `sct_footer`;
CREATE TABLE `sct_footer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_footer
-- ----------------------------
BEGIN;
INSERT INTO `sct_footer` VALUES (1, NULL);
COMMIT;

-- ----------------------------
-- Table structure for sct_logo_features
-- ----------------------------
DROP TABLE IF EXISTS `sct_logo_features`;
CREATE TABLE `sct_logo_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_logo_features
-- ----------------------------
BEGIN;
INSERT INTO `sct_logo_features` VALUES (1, 3, 'img-1.png');
INSERT INTO `sct_logo_features` VALUES (2, 3, 'img-2.png');
INSERT INTO `sct_logo_features` VALUES (3, 3, 'img-3.png');
INSERT INTO `sct_logo_features` VALUES (4, 3, 'img-4.png');
COMMIT;

-- ----------------------------
-- Table structure for sct_menu
-- ----------------------------
DROP TABLE IF EXISTS `sct_menu`;
CREATE TABLE `sct_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `link` enum('internal','external') DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `target` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_menu
-- ----------------------------
BEGIN;
INSERT INTO `sct_menu` VALUES (1, 1, 'HOME', 'internal', 'home', NULL);
INSERT INTO `sct_menu` VALUES (2, 1, 'ABOUT', 'internal', 'about', NULL);
INSERT INTO `sct_menu` VALUES (3, 1, 'SERVICES', 'internal', 'services', NULL);
INSERT INTO `sct_menu` VALUES (4, 1, 'NEWS', 'internal', 'news', NULL);
INSERT INTO `sct_menu` VALUES (5, 1, 'TESTIMONIALS', 'internal', 'testimonials', NULL);
INSERT INTO `sct_menu` VALUES (6, 1, 'CONTACT', 'internal', 'contact', NULL);
COMMIT;

-- ----------------------------
-- Table structure for sct_news
-- ----------------------------
DROP TABLE IF EXISTS `sct_news`;
CREATE TABLE `sct_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_news
-- ----------------------------
BEGIN;
INSERT INTO `sct_news` VALUES (1, 10, 'We are dedicated to satisfy clients', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blinds  and Separated they live in Bookmarksgrove right ');
COMMIT;

-- ----------------------------
-- Table structure for sct_news_items
-- ----------------------------
DROP TABLE IF EXISTS `sct_news_items`;
CREATE TABLE `sct_news_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sct_news_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_news_items
-- ----------------------------
BEGIN;
INSERT INTO `sct_news_items` VALUES (1, 1, 'img-1.jpg', 'Winning and retaining clients', '<p>Velg TE37 – Memodifikasi mobil nampaknya sudah menjadi kegiatan rutin bagi mereka para pecinta dunia otomotif, khususnya para modifikator. Tak lengkap rasanya bila mobil kesayanganya tidak di modif.</p>\n<p>Aliran modifikasi pun semakin banyak. Salah satunya memodifikasi mobil dengan aliran JDM. Aliran JDM ini memang sedang trend dimana – mana. Aliran modifikasi dengan velg JDM pun digemari dari berbagai kalangan, baik kalangan muda maupun mereka yang sudah tak muda.</p>\n<p>Untuk Sahabat Otto yang belum tahu, yang dimaksud aliran JDM adalah memodifikasi mobil dengan velg – velg keluaran Jepang. JDM sendiri merupakan kependekan dari Jepang Domestic Market.</p>\n<p>Salah satu modifikasi dengan aliran JDM hadir dari mobil SUV keluaran Honda, yaitu Honda BRV. Honda BR-V merupakan sebuah&nbsp;<a href=\"https://id.wikipedia.org/wiki/SUV\">SUV mini</a>&nbsp;yang diproduksi oleh&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda\">Honda</a>&nbsp;sejak tahun 2015. Sedang BR-V&nbsp;sendiri adalah singkatan dari&nbsp;<em>Bold&nbsp;Runabout&nbsp;Vehicle</em>.</p>\n<p>Mobil ini pertama kali diperkenalkan di&nbsp;<a href=\"https://id.wikipedia.org/wiki/Pameran_Mobil_Internasional_Indonesia\">Gaikindo Indonesia International Auto Show</a>&nbsp;2015 pada tanggal 20 Agustus 2015. Mobil ini kemudian mulai dipasarkan pada tanggal 23 Januari 2016. Honda BRV ini saling berbagi&nbsp;<em>platform</em>&nbsp;dari&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Brio\">Brio</a>,&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Mobilio\">Mobilio</a>, dan&nbsp;<a href=\"https://id.wikipedia.org/w/index.php?title=Honda_Amaze&amp;action=edit&amp;redlink=1\">Amaze</a>.</p>\n<p>Memodifikasi Honda BRV merupakan pilihan bagi mereka yang ingin membuat tampilan BRV-nya semakin keren dan gagah. Sahabat Otto bisa menggunakan velg racing untuk menunjang tampilan modifikasi mobil mu.</p>\n<p>Salah satu velg racing, JDM &nbsp;yang paling terkenal adalah velg TE37. Dimana velg volk racing TE37 ini mempunyai tampilan yang oke banget. Dijamin mobil tipe apapun jika dipasangkan velg TE37 ini akan berubah jadi makin ganteng dan keren!</p>\n<p>Dengan desain palang 6 yang simpel tapi abadi, velg TE37 ini langsung melejit pamornya setelah banyak dipakai di mobil-mobil sport dan balap.</p>\n<p>Velg TE37 memiliki beragam keunggulan, seperti desainnya yang legendaries juga&nbsp; tertua di kelasnya membuat Honda BRV mu semakin sporty dan elegan.</p>\n<p>Sahabat Otto bisa menggunakan velg TE37 ini dengan ukuran ring 17 dengan pcd 5×114,3 dan offset 38 dimana dengan ukuran tersebut velg TE37 ini juga bisa digunakan pada mobil – mobil lain seperti : Suzuki Ertiga, Subaru Impreza, Kijang Innova, Toyota Alphard, Toyota Camry, Honda Civic, Honda CRV, Wuling Cortez, Lexus dll.</p>\n<p>Apabila Sahabat Otto tertarik dengan velg TE37 ini kamu bisa lho langsung mendatangi toko kami di Jl. Ir H. Juanda No.63, Cemp. Putih, Ciputat, Kota Tangerang Selatan, Banten 15419 atau kamu juga bisa nih Sahabat Otto sekedar melihat –lihatnya di market place kami, seperti &nbsp;Tokopedia, Shopee atau Bukalapak.</p>\n				<div class=\"post_info post_info_bottom\">\n					<span class=\"post_info_item post_info_tags\"><span class=\"btag\">Tags:</span> <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda/\">honda</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda-brv/\">honda brv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-mobil/\">Jual Velg Mobil</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-te37/\">jual velg te37</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/ottoban-ciputat/\">Ottoban Ciputat</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/suv/\">suv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/velg-volk-racing/\">velg volk racing</a></span>\n				</div>', '2019-05-13 16:18:43');
INSERT INTO `sct_news_items` VALUES (2, 1, 'img-1.jpg', 'Winning and retaining clients', '<p>Velg TE37 – Memodifikasi mobil nampaknya sudah menjadi kegiatan rutin bagi mereka para pecinta dunia otomotif, khususnya para modifikator. Tak lengkap rasanya bila mobil kesayanganya tidak di modif.</p>\n<p>Aliran modifikasi pun semakin banyak. Salah satunya memodifikasi mobil dengan aliran JDM. Aliran JDM ini memang sedang trend dimana – mana. Aliran modifikasi dengan velg JDM pun digemari dari berbagai kalangan, baik kalangan muda maupun mereka yang sudah tak muda.</p>\n<p>Untuk Sahabat Otto yang belum tahu, yang dimaksud aliran JDM adalah memodifikasi mobil dengan velg – velg keluaran Jepang. JDM sendiri merupakan kependekan dari Jepang Domestic Market.</p>\n<p>Salah satu modifikasi dengan aliran JDM hadir dari mobil SUV keluaran Honda, yaitu Honda BRV. Honda BR-V merupakan sebuah&nbsp;<a href=\"https://id.wikipedia.org/wiki/SUV\">SUV mini</a>&nbsp;yang diproduksi oleh&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda\">Honda</a>&nbsp;sejak tahun 2015. Sedang BR-V&nbsp;sendiri adalah singkatan dari&nbsp;<em>Bold&nbsp;Runabout&nbsp;Vehicle</em>.</p>\n<p>Mobil ini pertama kali diperkenalkan di&nbsp;<a href=\"https://id.wikipedia.org/wiki/Pameran_Mobil_Internasional_Indonesia\">Gaikindo Indonesia International Auto Show</a>&nbsp;2015 pada tanggal 20 Agustus 2015. Mobil ini kemudian mulai dipasarkan pada tanggal 23 Januari 2016. Honda BRV ini saling berbagi&nbsp;<em>platform</em>&nbsp;dari&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Brio\">Brio</a>,&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Mobilio\">Mobilio</a>, dan&nbsp;<a href=\"https://id.wikipedia.org/w/index.php?title=Honda_Amaze&amp;action=edit&amp;redlink=1\">Amaze</a>.</p>\n<p>Memodifikasi Honda BRV merupakan pilihan bagi mereka yang ingin membuat tampilan BRV-nya semakin keren dan gagah. Sahabat Otto bisa menggunakan velg racing untuk menunjang tampilan modifikasi mobil mu.</p>\n<p>Salah satu velg racing, JDM &nbsp;yang paling terkenal adalah velg TE37. Dimana velg volk racing TE37 ini mempunyai tampilan yang oke banget. Dijamin mobil tipe apapun jika dipasangkan velg TE37 ini akan berubah jadi makin ganteng dan keren!</p>\n<p>Dengan desain palang 6 yang simpel tapi abadi, velg TE37 ini langsung melejit pamornya setelah banyak dipakai di mobil-mobil sport dan balap.</p>\n<p>Velg TE37 memiliki beragam keunggulan, seperti desainnya yang legendaries juga&nbsp; tertua di kelasnya membuat Honda BRV mu semakin sporty dan elegan.</p>\n<p>Sahabat Otto bisa menggunakan velg TE37 ini dengan ukuran ring 17 dengan pcd 5×114,3 dan offset 38 dimana dengan ukuran tersebut velg TE37 ini juga bisa digunakan pada mobil – mobil lain seperti : Suzuki Ertiga, Subaru Impreza, Kijang Innova, Toyota Alphard, Toyota Camry, Honda Civic, Honda CRV, Wuling Cortez, Lexus dll.</p>\n<p>Apabila Sahabat Otto tertarik dengan velg TE37 ini kamu bisa lho langsung mendatangi toko kami di Jl. Ir H. Juanda No.63, Cemp. Putih, Ciputat, Kota Tangerang Selatan, Banten 15419 atau kamu juga bisa nih Sahabat Otto sekedar melihat –lihatnya di market place kami, seperti &nbsp;Tokopedia, Shopee atau Bukalapak.</p>\n				<div class=\"post_info post_info_bottom\">\n					<span class=\"post_info_item post_info_tags\"><span class=\"btag\">Tags:</span> <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda/\">honda</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda-brv/\">honda brv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-mobil/\">Jual Velg Mobil</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-te37/\">jual velg te37</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/ottoban-ciputat/\">Ottoban Ciputat</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/suv/\">suv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/velg-volk-racing/\">velg volk racing</a></span>\n				</div>', '2019-05-13 16:18:43');
INSERT INTO `sct_news_items` VALUES (3, 1, 'img-1.jpg', 'Winning and retaining clients', '<p>Velg TE37 – Memodifikasi mobil nampaknya sudah menjadi kegiatan rutin bagi mereka para pecinta dunia otomotif, khususnya para modifikator. Tak lengkap rasanya bila mobil kesayanganya tidak di modif.</p>\n<p>Aliran modifikasi pun semakin banyak. Salah satunya memodifikasi mobil dengan aliran JDM. Aliran JDM ini memang sedang trend dimana – mana. Aliran modifikasi dengan velg JDM pun digemari dari berbagai kalangan, baik kalangan muda maupun mereka yang sudah tak muda.</p>\n<p>Untuk Sahabat Otto yang belum tahu, yang dimaksud aliran JDM adalah memodifikasi mobil dengan velg – velg keluaran Jepang. JDM sendiri merupakan kependekan dari Jepang Domestic Market.</p>\n<p>Salah satu modifikasi dengan aliran JDM hadir dari mobil SUV keluaran Honda, yaitu Honda BRV. Honda BR-V merupakan sebuah&nbsp;<a href=\"https://id.wikipedia.org/wiki/SUV\">SUV mini</a>&nbsp;yang diproduksi oleh&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda\">Honda</a>&nbsp;sejak tahun 2015. Sedang BR-V&nbsp;sendiri adalah singkatan dari&nbsp;<em>Bold&nbsp;Runabout&nbsp;Vehicle</em>.</p>\n<p>Mobil ini pertama kali diperkenalkan di&nbsp;<a href=\"https://id.wikipedia.org/wiki/Pameran_Mobil_Internasional_Indonesia\">Gaikindo Indonesia International Auto Show</a>&nbsp;2015 pada tanggal 20 Agustus 2015. Mobil ini kemudian mulai dipasarkan pada tanggal 23 Januari 2016. Honda BRV ini saling berbagi&nbsp;<em>platform</em>&nbsp;dari&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Brio\">Brio</a>,&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Mobilio\">Mobilio</a>, dan&nbsp;<a href=\"https://id.wikipedia.org/w/index.php?title=Honda_Amaze&amp;action=edit&amp;redlink=1\">Amaze</a>.</p>\n<p>Memodifikasi Honda BRV merupakan pilihan bagi mereka yang ingin membuat tampilan BRV-nya semakin keren dan gagah. Sahabat Otto bisa menggunakan velg racing untuk menunjang tampilan modifikasi mobil mu.</p>\n<p>Salah satu velg racing, JDM &nbsp;yang paling terkenal adalah velg TE37. Dimana velg volk racing TE37 ini mempunyai tampilan yang oke banget. Dijamin mobil tipe apapun jika dipasangkan velg TE37 ini akan berubah jadi makin ganteng dan keren!</p>\n<p>Dengan desain palang 6 yang simpel tapi abadi, velg TE37 ini langsung melejit pamornya setelah banyak dipakai di mobil-mobil sport dan balap.</p>\n<p>Velg TE37 memiliki beragam keunggulan, seperti desainnya yang legendaries juga&nbsp; tertua di kelasnya membuat Honda BRV mu semakin sporty dan elegan.</p>\n<p>Sahabat Otto bisa menggunakan velg TE37 ini dengan ukuran ring 17 dengan pcd 5×114,3 dan offset 38 dimana dengan ukuran tersebut velg TE37 ini juga bisa digunakan pada mobil – mobil lain seperti : Suzuki Ertiga, Subaru Impreza, Kijang Innova, Toyota Alphard, Toyota Camry, Honda Civic, Honda CRV, Wuling Cortez, Lexus dll.</p>\n<p>Apabila Sahabat Otto tertarik dengan velg TE37 ini kamu bisa lho langsung mendatangi toko kami di Jl. Ir H. Juanda No.63, Cemp. Putih, Ciputat, Kota Tangerang Selatan, Banten 15419 atau kamu juga bisa nih Sahabat Otto sekedar melihat –lihatnya di market place kami, seperti &nbsp;Tokopedia, Shopee atau Bukalapak.</p>\n				<div class=\"post_info post_info_bottom\">\n					<span class=\"post_info_item post_info_tags\"><span class=\"btag\">Tags:</span> <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda/\">honda</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda-brv/\">honda brv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-mobil/\">Jual Velg Mobil</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-te37/\">jual velg te37</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/ottoban-ciputat/\">Ottoban Ciputat</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/suv/\">suv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/velg-volk-racing/\">velg volk racing</a></span>\n				</div>', '2019-05-13 16:18:43');
INSERT INTO `sct_news_items` VALUES (4, 1, 'img-1.jpg', 'Winning and retaining clients', '<p>Velg TE37 – Memodifikasi mobil nampaknya sudah menjadi kegiatan rutin bagi mereka para pecinta dunia otomotif, khususnya para modifikator. Tak lengkap rasanya bila mobil kesayanganya tidak di modif.</p>\n<p>Aliran modifikasi pun semakin banyak. Salah satunya memodifikasi mobil dengan aliran JDM. Aliran JDM ini memang sedang trend dimana – mana. Aliran modifikasi dengan velg JDM pun digemari dari berbagai kalangan, baik kalangan muda maupun mereka yang sudah tak muda.</p>\n<p>Untuk Sahabat Otto yang belum tahu, yang dimaksud aliran JDM adalah memodifikasi mobil dengan velg – velg keluaran Jepang. JDM sendiri merupakan kependekan dari Jepang Domestic Market.</p>\n<p>Salah satu modifikasi dengan aliran JDM hadir dari mobil SUV keluaran Honda, yaitu Honda BRV. Honda BR-V merupakan sebuah&nbsp;<a href=\"https://id.wikipedia.org/wiki/SUV\">SUV mini</a>&nbsp;yang diproduksi oleh&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda\">Honda</a>&nbsp;sejak tahun 2015. Sedang BR-V&nbsp;sendiri adalah singkatan dari&nbsp;<em>Bold&nbsp;Runabout&nbsp;Vehicle</em>.</p>\n<p>Mobil ini pertama kali diperkenalkan di&nbsp;<a href=\"https://id.wikipedia.org/wiki/Pameran_Mobil_Internasional_Indonesia\">Gaikindo Indonesia International Auto Show</a>&nbsp;2015 pada tanggal 20 Agustus 2015. Mobil ini kemudian mulai dipasarkan pada tanggal 23 Januari 2016. Honda BRV ini saling berbagi&nbsp;<em>platform</em>&nbsp;dari&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Brio\">Brio</a>,&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Mobilio\">Mobilio</a>, dan&nbsp;<a href=\"https://id.wikipedia.org/w/index.php?title=Honda_Amaze&amp;action=edit&amp;redlink=1\">Amaze</a>.</p>\n<p>Memodifikasi Honda BRV merupakan pilihan bagi mereka yang ingin membuat tampilan BRV-nya semakin keren dan gagah. Sahabat Otto bisa menggunakan velg racing untuk menunjang tampilan modifikasi mobil mu.</p>\n<p>Salah satu velg racing, JDM &nbsp;yang paling terkenal adalah velg TE37. Dimana velg volk racing TE37 ini mempunyai tampilan yang oke banget. Dijamin mobil tipe apapun jika dipasangkan velg TE37 ini akan berubah jadi makin ganteng dan keren!</p>\n<p>Dengan desain palang 6 yang simpel tapi abadi, velg TE37 ini langsung melejit pamornya setelah banyak dipakai di mobil-mobil sport dan balap.</p>\n<p>Velg TE37 memiliki beragam keunggulan, seperti desainnya yang legendaries juga&nbsp; tertua di kelasnya membuat Honda BRV mu semakin sporty dan elegan.</p>\n<p>Sahabat Otto bisa menggunakan velg TE37 ini dengan ukuran ring 17 dengan pcd 5×114,3 dan offset 38 dimana dengan ukuran tersebut velg TE37 ini juga bisa digunakan pada mobil – mobil lain seperti : Suzuki Ertiga, Subaru Impreza, Kijang Innova, Toyota Alphard, Toyota Camry, Honda Civic, Honda CRV, Wuling Cortez, Lexus dll.</p>\n<p>Apabila Sahabat Otto tertarik dengan velg TE37 ini kamu bisa lho langsung mendatangi toko kami di Jl. Ir H. Juanda No.63, Cemp. Putih, Ciputat, Kota Tangerang Selatan, Banten 15419 atau kamu juga bisa nih Sahabat Otto sekedar melihat –lihatnya di market place kami, seperti &nbsp;Tokopedia, Shopee atau Bukalapak.</p>\n				<div class=\"post_info post_info_bottom\">\n					<span class=\"post_info_item post_info_tags\"><span class=\"btag\">Tags:</span> <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda/\">honda</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda-brv/\">honda brv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-mobil/\">Jual Velg Mobil</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-te37/\">jual velg te37</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/ottoban-ciputat/\">Ottoban Ciputat</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/suv/\">suv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/velg-volk-racing/\">velg volk racing</a></span>\n				</div>', '2019-05-13 16:18:43');
INSERT INTO `sct_news_items` VALUES (5, 1, 'img-1.jpg', 'Winning and retaining clients', '<p>Velg TE37 – Memodifikasi mobil nampaknya sudah menjadi kegiatan rutin bagi mereka para pecinta dunia otomotif, khususnya para modifikator. Tak lengkap rasanya bila mobil kesayanganya tidak di modif.</p>\n<p>Aliran modifikasi pun semakin banyak. Salah satunya memodifikasi mobil dengan aliran JDM. Aliran JDM ini memang sedang trend dimana – mana. Aliran modifikasi dengan velg JDM pun digemari dari berbagai kalangan, baik kalangan muda maupun mereka yang sudah tak muda.</p>\n<p>Untuk Sahabat Otto yang belum tahu, yang dimaksud aliran JDM adalah memodifikasi mobil dengan velg – velg keluaran Jepang. JDM sendiri merupakan kependekan dari Jepang Domestic Market.</p>\n<p>Salah satu modifikasi dengan aliran JDM hadir dari mobil SUV keluaran Honda, yaitu Honda BRV. Honda BR-V merupakan sebuah&nbsp;<a href=\"https://id.wikipedia.org/wiki/SUV\">SUV mini</a>&nbsp;yang diproduksi oleh&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda\">Honda</a>&nbsp;sejak tahun 2015. Sedang BR-V&nbsp;sendiri adalah singkatan dari&nbsp;<em>Bold&nbsp;Runabout&nbsp;Vehicle</em>.</p>\n<p>Mobil ini pertama kali diperkenalkan di&nbsp;<a href=\"https://id.wikipedia.org/wiki/Pameran_Mobil_Internasional_Indonesia\">Gaikindo Indonesia International Auto Show</a>&nbsp;2015 pada tanggal 20 Agustus 2015. Mobil ini kemudian mulai dipasarkan pada tanggal 23 Januari 2016. Honda BRV ini saling berbagi&nbsp;<em>platform</em>&nbsp;dari&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Brio\">Brio</a>,&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Mobilio\">Mobilio</a>, dan&nbsp;<a href=\"https://id.wikipedia.org/w/index.php?title=Honda_Amaze&amp;action=edit&amp;redlink=1\">Amaze</a>.</p>\n<p>Memodifikasi Honda BRV merupakan pilihan bagi mereka yang ingin membuat tampilan BRV-nya semakin keren dan gagah. Sahabat Otto bisa menggunakan velg racing untuk menunjang tampilan modifikasi mobil mu.</p>\n<p>Salah satu velg racing, JDM &nbsp;yang paling terkenal adalah velg TE37. Dimana velg volk racing TE37 ini mempunyai tampilan yang oke banget. Dijamin mobil tipe apapun jika dipasangkan velg TE37 ini akan berubah jadi makin ganteng dan keren!</p>\n<p>Dengan desain palang 6 yang simpel tapi abadi, velg TE37 ini langsung melejit pamornya setelah banyak dipakai di mobil-mobil sport dan balap.</p>\n<p>Velg TE37 memiliki beragam keunggulan, seperti desainnya yang legendaries juga&nbsp; tertua di kelasnya membuat Honda BRV mu semakin sporty dan elegan.</p>\n<p>Sahabat Otto bisa menggunakan velg TE37 ini dengan ukuran ring 17 dengan pcd 5×114,3 dan offset 38 dimana dengan ukuran tersebut velg TE37 ini juga bisa digunakan pada mobil – mobil lain seperti : Suzuki Ertiga, Subaru Impreza, Kijang Innova, Toyota Alphard, Toyota Camry, Honda Civic, Honda CRV, Wuling Cortez, Lexus dll.</p>\n<p>Apabila Sahabat Otto tertarik dengan velg TE37 ini kamu bisa lho langsung mendatangi toko kami di Jl. Ir H. Juanda No.63, Cemp. Putih, Ciputat, Kota Tangerang Selatan, Banten 15419 atau kamu juga bisa nih Sahabat Otto sekedar melihat –lihatnya di market place kami, seperti &nbsp;Tokopedia, Shopee atau Bukalapak.</p>\n				<div class=\"post_info post_info_bottom\">\n					<span class=\"post_info_item post_info_tags\"><span class=\"btag\">Tags:</span> <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda/\">honda</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda-brv/\">honda brv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-mobil/\">Jual Velg Mobil</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-te37/\">jual velg te37</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/ottoban-ciputat/\">Ottoban Ciputat</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/suv/\">suv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/velg-volk-racing/\">velg volk racing</a></span>\n				</div>', '2019-05-13 16:18:43');
INSERT INTO `sct_news_items` VALUES (6, 1, 'img-1.jpg', 'Winning and retaining clients', '<p>Velg TE37 – Memodifikasi mobil nampaknya sudah menjadi kegiatan rutin bagi mereka para pecinta dunia otomotif, khususnya para modifikator. Tak lengkap rasanya bila mobil kesayanganya tidak di modif.</p>\n<p>Aliran modifikasi pun semakin banyak. Salah satunya memodifikasi mobil dengan aliran JDM. Aliran JDM ini memang sedang trend dimana – mana. Aliran modifikasi dengan velg JDM pun digemari dari berbagai kalangan, baik kalangan muda maupun mereka yang sudah tak muda.</p>\n<p>Untuk Sahabat Otto yang belum tahu, yang dimaksud aliran JDM adalah memodifikasi mobil dengan velg – velg keluaran Jepang. JDM sendiri merupakan kependekan dari Jepang Domestic Market.</p>\n<p>Salah satu modifikasi dengan aliran JDM hadir dari mobil SUV keluaran Honda, yaitu Honda BRV. Honda BR-V merupakan sebuah&nbsp;<a href=\"https://id.wikipedia.org/wiki/SUV\">SUV mini</a>&nbsp;yang diproduksi oleh&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda\">Honda</a>&nbsp;sejak tahun 2015. Sedang BR-V&nbsp;sendiri adalah singkatan dari&nbsp;<em>Bold&nbsp;Runabout&nbsp;Vehicle</em>.</p>\n<p>Mobil ini pertama kali diperkenalkan di&nbsp;<a href=\"https://id.wikipedia.org/wiki/Pameran_Mobil_Internasional_Indonesia\">Gaikindo Indonesia International Auto Show</a>&nbsp;2015 pada tanggal 20 Agustus 2015. Mobil ini kemudian mulai dipasarkan pada tanggal 23 Januari 2016. Honda BRV ini saling berbagi&nbsp;<em>platform</em>&nbsp;dari&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Brio\">Brio</a>,&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Mobilio\">Mobilio</a>, dan&nbsp;<a href=\"https://id.wikipedia.org/w/index.php?title=Honda_Amaze&amp;action=edit&amp;redlink=1\">Amaze</a>.</p>\n<p>Memodifikasi Honda BRV merupakan pilihan bagi mereka yang ingin membuat tampilan BRV-nya semakin keren dan gagah. Sahabat Otto bisa menggunakan velg racing untuk menunjang tampilan modifikasi mobil mu.</p>\n<p>Salah satu velg racing, JDM &nbsp;yang paling terkenal adalah velg TE37. Dimana velg volk racing TE37 ini mempunyai tampilan yang oke banget. Dijamin mobil tipe apapun jika dipasangkan velg TE37 ini akan berubah jadi makin ganteng dan keren!</p>\n<p>Dengan desain palang 6 yang simpel tapi abadi, velg TE37 ini langsung melejit pamornya setelah banyak dipakai di mobil-mobil sport dan balap.</p>\n<p>Velg TE37 memiliki beragam keunggulan, seperti desainnya yang legendaries juga&nbsp; tertua di kelasnya membuat Honda BRV mu semakin sporty dan elegan.</p>\n<p>Sahabat Otto bisa menggunakan velg TE37 ini dengan ukuran ring 17 dengan pcd 5×114,3 dan offset 38 dimana dengan ukuran tersebut velg TE37 ini juga bisa digunakan pada mobil – mobil lain seperti : Suzuki Ertiga, Subaru Impreza, Kijang Innova, Toyota Alphard, Toyota Camry, Honda Civic, Honda CRV, Wuling Cortez, Lexus dll.</p>\n<p>Apabila Sahabat Otto tertarik dengan velg TE37 ini kamu bisa lho langsung mendatangi toko kami di Jl. Ir H. Juanda No.63, Cemp. Putih, Ciputat, Kota Tangerang Selatan, Banten 15419 atau kamu juga bisa nih Sahabat Otto sekedar melihat –lihatnya di market place kami, seperti &nbsp;Tokopedia, Shopee atau Bukalapak.</p>\n				<div class=\"post_info post_info_bottom\">\n					<span class=\"post_info_item post_info_tags\"><span class=\"btag\">Tags:</span> <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda/\">honda</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda-brv/\">honda brv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-mobil/\">Jual Velg Mobil</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-te37/\">jual velg te37</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/ottoban-ciputat/\">Ottoban Ciputat</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/suv/\">suv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/velg-volk-racing/\">velg volk racing</a></span>\n				</div>', '2019-05-13 16:18:43');
INSERT INTO `sct_news_items` VALUES (7, 1, 'img-1.jpg', 'Winning and retaining clients', '<p>Velg TE37 – Memodifikasi mobil nampaknya sudah menjadi kegiatan rutin bagi mereka para pecinta dunia otomotif, khususnya para modifikator. Tak lengkap rasanya bila mobil kesayanganya tidak di modif.</p>\n<p>Aliran modifikasi pun semakin banyak. Salah satunya memodifikasi mobil dengan aliran JDM. Aliran JDM ini memang sedang trend dimana – mana. Aliran modifikasi dengan velg JDM pun digemari dari berbagai kalangan, baik kalangan muda maupun mereka yang sudah tak muda.</p>\n<p>Untuk Sahabat Otto yang belum tahu, yang dimaksud aliran JDM adalah memodifikasi mobil dengan velg – velg keluaran Jepang. JDM sendiri merupakan kependekan dari Jepang Domestic Market.</p>\n<p>Salah satu modifikasi dengan aliran JDM hadir dari mobil SUV keluaran Honda, yaitu Honda BRV. Honda BR-V merupakan sebuah&nbsp;<a href=\"https://id.wikipedia.org/wiki/SUV\">SUV mini</a>&nbsp;yang diproduksi oleh&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda\">Honda</a>&nbsp;sejak tahun 2015. Sedang BR-V&nbsp;sendiri adalah singkatan dari&nbsp;<em>Bold&nbsp;Runabout&nbsp;Vehicle</em>.</p>\n<p>Mobil ini pertama kali diperkenalkan di&nbsp;<a href=\"https://id.wikipedia.org/wiki/Pameran_Mobil_Internasional_Indonesia\">Gaikindo Indonesia International Auto Show</a>&nbsp;2015 pada tanggal 20 Agustus 2015. Mobil ini kemudian mulai dipasarkan pada tanggal 23 Januari 2016. Honda BRV ini saling berbagi&nbsp;<em>platform</em>&nbsp;dari&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Brio\">Brio</a>,&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Mobilio\">Mobilio</a>, dan&nbsp;<a href=\"https://id.wikipedia.org/w/index.php?title=Honda_Amaze&amp;action=edit&amp;redlink=1\">Amaze</a>.</p>\n<p>Memodifikasi Honda BRV merupakan pilihan bagi mereka yang ingin membuat tampilan BRV-nya semakin keren dan gagah. Sahabat Otto bisa menggunakan velg racing untuk menunjang tampilan modifikasi mobil mu.</p>\n<p>Salah satu velg racing, JDM &nbsp;yang paling terkenal adalah velg TE37. Dimana velg volk racing TE37 ini mempunyai tampilan yang oke banget. Dijamin mobil tipe apapun jika dipasangkan velg TE37 ini akan berubah jadi makin ganteng dan keren!</p>\n<p>Dengan desain palang 6 yang simpel tapi abadi, velg TE37 ini langsung melejit pamornya setelah banyak dipakai di mobil-mobil sport dan balap.</p>\n<p>Velg TE37 memiliki beragam keunggulan, seperti desainnya yang legendaries juga&nbsp; tertua di kelasnya membuat Honda BRV mu semakin sporty dan elegan.</p>\n<p>Sahabat Otto bisa menggunakan velg TE37 ini dengan ukuran ring 17 dengan pcd 5×114,3 dan offset 38 dimana dengan ukuran tersebut velg TE37 ini juga bisa digunakan pada mobil – mobil lain seperti : Suzuki Ertiga, Subaru Impreza, Kijang Innova, Toyota Alphard, Toyota Camry, Honda Civic, Honda CRV, Wuling Cortez, Lexus dll.</p>\n<p>Apabila Sahabat Otto tertarik dengan velg TE37 ini kamu bisa lho langsung mendatangi toko kami di Jl. Ir H. Juanda No.63, Cemp. Putih, Ciputat, Kota Tangerang Selatan, Banten 15419 atau kamu juga bisa nih Sahabat Otto sekedar melihat –lihatnya di market place kami, seperti &nbsp;Tokopedia, Shopee atau Bukalapak.</p>\n				<div class=\"post_info post_info_bottom\">\n					<span class=\"post_info_item post_info_tags\"><span class=\"btag\">Tags:</span> <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda/\">honda</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda-brv/\">honda brv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-mobil/\">Jual Velg Mobil</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-te37/\">jual velg te37</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/ottoban-ciputat/\">Ottoban Ciputat</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/suv/\">suv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/velg-volk-racing/\">velg volk racing</a></span>\n				</div>', '2019-05-13 16:18:43');
INSERT INTO `sct_news_items` VALUES (8, 1, 'img-1.jpg', 'Winning and retaining clients', '<p>Velg TE37 – Memodifikasi mobil nampaknya sudah menjadi kegiatan rutin bagi mereka para pecinta dunia otomotif, khususnya para modifikator. Tak lengkap rasanya bila mobil kesayanganya tidak di modif.</p>\n<p>Aliran modifikasi pun semakin banyak. Salah satunya memodifikasi mobil dengan aliran JDM. Aliran JDM ini memang sedang trend dimana – mana. Aliran modifikasi dengan velg JDM pun digemari dari berbagai kalangan, baik kalangan muda maupun mereka yang sudah tak muda.</p>\n<p>Untuk Sahabat Otto yang belum tahu, yang dimaksud aliran JDM adalah memodifikasi mobil dengan velg – velg keluaran Jepang. JDM sendiri merupakan kependekan dari Jepang Domestic Market.</p>\n<p>Salah satu modifikasi dengan aliran JDM hadir dari mobil SUV keluaran Honda, yaitu Honda BRV. Honda BR-V merupakan sebuah&nbsp;<a href=\"https://id.wikipedia.org/wiki/SUV\">SUV mini</a>&nbsp;yang diproduksi oleh&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda\">Honda</a>&nbsp;sejak tahun 2015. Sedang BR-V&nbsp;sendiri adalah singkatan dari&nbsp;<em>Bold&nbsp;Runabout&nbsp;Vehicle</em>.</p>\n<p>Mobil ini pertama kali diperkenalkan di&nbsp;<a href=\"https://id.wikipedia.org/wiki/Pameran_Mobil_Internasional_Indonesia\">Gaikindo Indonesia International Auto Show</a>&nbsp;2015 pada tanggal 20 Agustus 2015. Mobil ini kemudian mulai dipasarkan pada tanggal 23 Januari 2016. Honda BRV ini saling berbagi&nbsp;<em>platform</em>&nbsp;dari&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Brio\">Brio</a>,&nbsp;<a href=\"https://id.wikipedia.org/wiki/Honda_Mobilio\">Mobilio</a>, dan&nbsp;<a href=\"https://id.wikipedia.org/w/index.php?title=Honda_Amaze&amp;action=edit&amp;redlink=1\">Amaze</a>.</p>\n<p>Memodifikasi Honda BRV merupakan pilihan bagi mereka yang ingin membuat tampilan BRV-nya semakin keren dan gagah. Sahabat Otto bisa menggunakan velg racing untuk menunjang tampilan modifikasi mobil mu.</p>\n<p>Salah satu velg racing, JDM &nbsp;yang paling terkenal adalah velg TE37. Dimana velg volk racing TE37 ini mempunyai tampilan yang oke banget. Dijamin mobil tipe apapun jika dipasangkan velg TE37 ini akan berubah jadi makin ganteng dan keren!</p>\n<p>Dengan desain palang 6 yang simpel tapi abadi, velg TE37 ini langsung melejit pamornya setelah banyak dipakai di mobil-mobil sport dan balap.</p>\n<p>Velg TE37 memiliki beragam keunggulan, seperti desainnya yang legendaries juga&nbsp; tertua di kelasnya membuat Honda BRV mu semakin sporty dan elegan.</p>\n<p>Sahabat Otto bisa menggunakan velg TE37 ini dengan ukuran ring 17 dengan pcd 5×114,3 dan offset 38 dimana dengan ukuran tersebut velg TE37 ini juga bisa digunakan pada mobil – mobil lain seperti : Suzuki Ertiga, Subaru Impreza, Kijang Innova, Toyota Alphard, Toyota Camry, Honda Civic, Honda CRV, Wuling Cortez, Lexus dll.</p>\n<p>Apabila Sahabat Otto tertarik dengan velg TE37 ini kamu bisa lho langsung mendatangi toko kami di Jl. Ir H. Juanda No.63, Cemp. Putih, Ciputat, Kota Tangerang Selatan, Banten 15419 atau kamu juga bisa nih Sahabat Otto sekedar melihat –lihatnya di market place kami, seperti &nbsp;Tokopedia, Shopee atau Bukalapak.</p>\n				<div class=\"post_info post_info_bottom\">\n					<span class=\"post_info_item post_info_tags\"><span class=\"btag\">Tags:</span> <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda/\">honda</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/honda-brv/\">honda brv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-mobil/\">Jual Velg Mobil</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/jual-velg-te37/\">jual velg te37</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/ottoban-ciputat/\">Ottoban Ciputat</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/suv/\">suv</a>, <a class=\"post_tag_link\" href=\"http://jualbelivelg.com/tag/velg-volk-racing/\">velg volk racing</a></span>\n				</div>', '2019-05-13 16:18:43');
COMMIT;

-- ----------------------------
-- Table structure for sct_product
-- ----------------------------
DROP TABLE IF EXISTS `sct_product`;
CREATE TABLE `sct_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_product
-- ----------------------------
BEGIN;
INSERT INTO `sct_product` VALUES (1, 8, 'Project which make us different', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blinds and Separated they live in Bookmarksgrove right');
COMMIT;

-- ----------------------------
-- Table structure for sct_product_items
-- ----------------------------
DROP TABLE IF EXISTS `sct_product_items`;
CREATE TABLE `sct_product_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sct_product_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `price` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_product_items
-- ----------------------------
BEGIN;
INSERT INTO `sct_product_items` VALUES (1, 1, 'img-4.jpg', 'TE37 Ring 17 PCD', 4700000);
INSERT INTO `sct_product_items` VALUES (2, 1, 'img-4.jpg', 'TE37 Ring 17 PCD', 4700000);
INSERT INTO `sct_product_items` VALUES (3, 1, 'img-4.jpg', 'TE37 Ring 17 PCD', 4700000);
INSERT INTO `sct_product_items` VALUES (4, 1, 'img-4.jpg', 'TE37 Ring 17 PCD', 4700000);
COMMIT;

-- ----------------------------
-- Table structure for sct_services
-- ----------------------------
DROP TABLE IF EXISTS `sct_services`;
CREATE TABLE `sct_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_services
-- ----------------------------
BEGIN;
INSERT INTO `sct_services` VALUES (1, 5, 'ico1.png', 'Photographer', 'We use a customized machine been to specifically designed to test diagnose any and all automotive.');
INSERT INTO `sct_services` VALUES (2, 5, 'ico1.png', 'Photographer', 'We use a customized machine been to specifically designed to test diagnose any and all automotive.');
INSERT INTO `sct_services` VALUES (3, 5, 'ico1.png', 'Photographer', 'We use a customized machine been to specifically designed to test diagnose any and all automotive.');
COMMIT;

-- ----------------------------
-- Table structure for sct_services_fun_fact
-- ----------------------------
DROP TABLE IF EXISTS `sct_services_fun_fact`;
CREATE TABLE `sct_services_fun_fact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `number` int(10) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_services_fun_fact
-- ----------------------------
BEGIN;
INSERT INTO `sct_services_fun_fact` VALUES (1, 7, 'icon-1.png', 856, 'Line code');
INSERT INTO `sct_services_fun_fact` VALUES (2, 7, 'icon-2.png', 10000, 'Active User');
INSERT INTO `sct_services_fun_fact` VALUES (3, 7, 'icon-3.png', 185, 'Download');
INSERT INTO `sct_services_fun_fact` VALUES (4, 7, 'icon-4.png', 54, 'Positive Rating');
COMMIT;

-- ----------------------------
-- Table structure for sct_showcase_hero
-- ----------------------------
DROP TABLE IF EXISTS `sct_showcase_hero`;
CREATE TABLE `sct_showcase_hero` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sub_title_1` varchar(255) DEFAULT NULL,
  `sub_title_2` varchar(255) DEFAULT NULL,
  `sub_title_3` varchar(255) DEFAULT NULL,
  `typed_string` text,
  `button_type` enum('internal','external') DEFAULT NULL,
  `button_link` varchar(255) DEFAULT NULL,
  `button_text` text,
  `background_image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_showcase_hero
-- ----------------------------
BEGIN;
INSERT INTO `sct_showcase_hero` VALUES (1, 2, 'Awesome Quality Of Management', 'Get Service Of', 'Galaxy.', ' For Best ', '[\"agus\",\"dwi\",\"parayogo\"]', 'internal', 'about', 'Discover us ', 'hero-bg.jpg');
COMMIT;

-- ----------------------------
-- Table structure for sct_testimonial
-- ----------------------------
DROP TABLE IF EXISTS `sct_testimonial`;
CREATE TABLE `sct_testimonial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_testimonial
-- ----------------------------
BEGIN;
INSERT INTO `sct_testimonial` VALUES (1, 11, 'We are dedicated to satisfy clients', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blinds  and Separated they live in Bookmarksgrove right ');
COMMIT;

-- ----------------------------
-- Table structure for sct_testimonial_items
-- ----------------------------
DROP TABLE IF EXISTS `sct_testimonial_items`;
CREATE TABLE `sct_testimonial_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sct_testimonial_id` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `testi` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sct_testimonial_items
-- ----------------------------
BEGIN;
INSERT INTO `sct_testimonial_items` VALUES (1, 1, 'img-1.jpg', 'Tahira Syarif', 'Marcco', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blinds and Separated they live in Bookmarksgrove right');
INSERT INTO `sct_testimonial_items` VALUES (2, 1, 'img-1.jpg', 'Tahira Syarif', 'Marcco', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blinds and Separated they live in Bookmarksgrove right');
INSERT INTO `sct_testimonial_items` VALUES (3, 1, 'img-1.jpg', 'Tahira Syarif', 'Marcco', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blinds and Separated they live in Bookmarksgrove right');
COMMIT;

-- ----------------------------
-- Table structure for section_list
-- ----------------------------
DROP TABLE IF EXISTS `section_list`;
CREATE TABLE `section_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory` varchar(100) COLLATE utf8_unicode_ci DEFAULT '_global',
  `section_id` int(11) NOT NULL,
  `type_sec_id` int(11) NOT NULL,
  `sec_order` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of section_list
-- ----------------------------
BEGIN;
INSERT INTO `section_list` VALUES (1, '_global', 1, 1, 1, '2019-05-11 10:52:27');
INSERT INTO `section_list` VALUES (2, '_global', 1, 2, 2, '2019-05-11 10:52:45');
INSERT INTO `section_list` VALUES (3, '_global', 1, 3, 3, '2019-05-11 10:52:52');
INSERT INTO `section_list` VALUES (4, '_global', 1, 4, 4, '2019-05-11 10:52:57');
INSERT INTO `section_list` VALUES (5, '_global', 1, 5, 5, '2019-05-11 10:53:02');
INSERT INTO `section_list` VALUES (6, '_global', 1, 6, 6, '2019-05-11 10:53:07');
INSERT INTO `section_list` VALUES (7, '_global', 1, 7, 7, '2019-05-11 10:53:13');
INSERT INTO `section_list` VALUES (8, '_global', 1, 8, 8, '2019-05-11 10:53:18');
INSERT INTO `section_list` VALUES (9, '_global', 1, 9, 9, '2019-05-11 10:53:21');
INSERT INTO `section_list` VALUES (10, '_global', 1, 10, 10, '2019-05-11 10:53:25');
INSERT INTO `section_list` VALUES (11, '_global', 1, 11, 11, '2019-05-11 10:53:29');
INSERT INTO `section_list` VALUES (12, '_global', 1, 12, 12, '2019-05-11 10:53:33');
INSERT INTO `section_list` VALUES (13, '_global', 1, 13, 13, '2019-05-11 10:53:37');
COMMIT;

-- ----------------------------
-- Table structure for section_type
-- ----------------------------
DROP TABLE IF EXISTS `section_type`;
CREATE TABLE `section_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `db_table` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `menu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of section_type
-- ----------------------------
BEGIN;
INSERT INTO `section_type` VALUES (1, 'menu', 'sct_menu', 'menu');
INSERT INTO `section_type` VALUES (2, 'showcase hero', 'sct_showcase_hero', 'home');
INSERT INTO `section_type` VALUES (3, 'logo features', 'sct_logo_features', 'logo_features');
INSERT INTO `section_type` VALUES (4, 'about', 'sct_about', 'about');
INSERT INTO `section_type` VALUES (5, 'services', 'sct_services', 'services');
INSERT INTO `section_type` VALUES (6, 'about background', 'sct_about_bg', 'about_bg');
INSERT INTO `section_type` VALUES (7, 'services fun fact', 'sct_services_fun_fact', 'services_fun_fact');
INSERT INTO `section_type` VALUES (8, 'product', 'sct_product', 'product');
INSERT INTO `section_type` VALUES (9, 'faq accordion', 'sct_faq_accordion', 'faq');
INSERT INTO `section_type` VALUES (10, 'news', 'sct_news', 'news');
INSERT INTO `section_type` VALUES (11, 'testimonial', 'sct_testimonial', 'testimonial');
INSERT INTO `section_type` VALUES (12, 'contact', 'sct_contact', 'contact');
INSERT INTO `section_type` VALUES (13, 'footer', 'sct_footer', 'bottom_footer');
COMMIT;

-- ----------------------------
-- Table structure for site_config
-- ----------------------------
DROP TABLE IF EXISTS `site_config`;
CREATE TABLE `site_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subdomain` varchar(100) DEFAULT 'www',
  `name` varchar(255) DEFAULT NULL,
  `value` text,
  `value2` varchar(255) DEFAULT NULL,
  `value3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of site_config
-- ----------------------------
BEGIN;
INSERT INTO `site_config` VALUES (1, 'www', 'site-title', 'Nexbay.id', NULL, NULL);
INSERT INTO `site_config` VALUES (2, 'www', 'top-title', '', NULL, NULL);
INSERT INTO `site_config` VALUES (3, 'www', 'top-subtitle', '', NULL, NULL);
INSERT INTO `site_config` VALUES (4, 'www', 'meta-description', 'Original Mini Speaker Bluetooth', NULL, NULL);
INSERT INTO `site_config` VALUES (5, 'www', 'meta-keywords', 'Jual Mini Speaker Bluetooth', NULL, NULL);
INSERT INTO `site_config` VALUES (6, 'www', 'fav-icon', NULL, NULL, NULL);
INSERT INTO `site_config` VALUES (7, 'www', 'buy-now', 'footer', 'on', 'HUBUNGI KAMI');
INSERT INTO `site_config` VALUES (8, 'www', 'email-footer', 'Jika butuh bantuan silahkan hubungi Whatsapp : \n<a href=\"https://api.whatsapp.com/send?phone=+6281585023190&text=Halo%20Admin%20Saya%20Mau%20Order%20Produk%20Dari%20Nexbay%20Store\">+6281585023190	</a>', NULL, NULL);
INSERT INTO `site_config` VALUES (9, 'www', 'user-buy', '950', '', NULL);
INSERT INTO `site_config` VALUES (10, 'www', 'user-see-bottom', '1100', '100', NULL);
INSERT INTO `site_config` VALUES (11, 'www', 'user-see-top', '1200', '100', NULL);
INSERT INTO `site_config` VALUES (12, 'www', 'fb-track', '<!-- Facebook Pixel Code -->\n<script>\n  !function(f,b,e,v,n,t,s)\n  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?\n  n.callMethod.apply(n,arguments):n.queue.push(arguments)};\n  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version=\'2.0\';\n  n.queue=[];t=b.createElement(e);t.async=!0;\n  t.src=v;s=b.getElementsByTagName(e)[0];\n  s.parentNode.insertBefore(t,s)}(window, document,\'script\',\n  \'https://connect.facebook.net/en_US/fbevents.js\');\n  fbq(\'init\', \'2101106623318900\');\n  fbq(\'track\', \'PageView\');\n</script>\n<noscript><img height=\"1\" width=\"1\" style=\"display:none\"\n  src=\"https://www.facebook.com/tr?id=2101106623318900&ev=PageView&noscript=1\"\n/></noscript>\n<!-- End Facebook Pixel Code -->', NULL, NULL);
INSERT INTO `site_config` VALUES (13, 'www', 'google-track', '<!-- Global site tag (gtag.js) - Google Analytics -->\n<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-44202569-8\"></script>\n<script>\n  window.dataLayer = window.dataLayer || [];\n  function gtag(){dataLayer.push(arguments);}\n  gtag(\'js\', new Date());\n\n  gtag(\'config\', \'UA-44202569-8\');\n</script>\n', NULL, NULL);
INSERT INTO `site_config` VALUES (14, 'www', 'pop-ads', NULL, NULL, NULL);
INSERT INTO `site_config` VALUES (15, 'www', 'reserved-2', NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for visitor
-- ----------------------------
DROP TABLE IF EXISTS `visitor`;
CREATE TABLE `visitor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subdomain` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'www',
  `ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of visitor
-- ----------------------------
BEGIN;
INSERT INTO `visitor` VALUES (1, 'www', '::1', '2019-05-11 11:00:13');
INSERT INTO `visitor` VALUES (2, 'www', '::1', '2019-05-11 11:01:18');
INSERT INTO `visitor` VALUES (3, 'www', '::1', '2019-05-11 11:01:19');
INSERT INTO `visitor` VALUES (4, 'www', '::1', '2019-05-11 11:01:19');
INSERT INTO `visitor` VALUES (5, 'www', '::1', '2019-05-11 11:01:20');
INSERT INTO `visitor` VALUES (6, 'www', '::1', '2019-05-11 11:01:20');
INSERT INTO `visitor` VALUES (7, 'www', '::1', '2019-05-11 11:01:20');
INSERT INTO `visitor` VALUES (8, 'www', '::1', '2019-05-11 11:01:21');
INSERT INTO `visitor` VALUES (9, 'www', '::1', '2019-05-11 11:01:21');
INSERT INTO `visitor` VALUES (10, 'www', '::1', '2019-05-11 11:01:21');
INSERT INTO `visitor` VALUES (11, 'www', '::1', '2019-05-11 11:01:21');
INSERT INTO `visitor` VALUES (12, 'www', '::1', '2019-05-11 11:01:53');
INSERT INTO `visitor` VALUES (13, 'www', '::1', '2019-05-11 11:01:54');
INSERT INTO `visitor` VALUES (14, 'www', '::1', '2019-05-11 11:01:55');
INSERT INTO `visitor` VALUES (15, 'www', '::1', '2019-05-11 11:15:11');
INSERT INTO `visitor` VALUES (16, 'www', '::1', '2019-05-11 11:15:13');
INSERT INTO `visitor` VALUES (17, 'www', '::1', '2019-05-11 11:15:29');
INSERT INTO `visitor` VALUES (18, 'www', '::1', '2019-05-11 11:15:30');
INSERT INTO `visitor` VALUES (19, 'www', '::1', '2019-05-11 11:15:37');
INSERT INTO `visitor` VALUES (20, 'www', '::1', '2019-05-11 11:16:04');
INSERT INTO `visitor` VALUES (21, 'www', '::1', '2019-05-11 11:17:37');
INSERT INTO `visitor` VALUES (22, 'www', '::1', '2019-05-11 11:22:05');
INSERT INTO `visitor` VALUES (23, 'www', '::1', '2019-05-11 11:22:06');
INSERT INTO `visitor` VALUES (24, 'www', '::1', '2019-05-11 11:22:10');
INSERT INTO `visitor` VALUES (25, 'www', '::1', '2019-05-11 11:22:43');
INSERT INTO `visitor` VALUES (26, 'www', '::1', '2019-05-11 11:23:20');
INSERT INTO `visitor` VALUES (27, 'www', '::1', '2019-05-11 11:23:22');
INSERT INTO `visitor` VALUES (28, 'www', '::1', '2019-05-11 11:23:22');
INSERT INTO `visitor` VALUES (29, 'www', '::1', '2019-05-11 11:23:23');
INSERT INTO `visitor` VALUES (30, 'www', '::1', '2019-05-11 11:23:23');
INSERT INTO `visitor` VALUES (31, '0', '::1', '2019-05-11 11:25:24');
INSERT INTO `visitor` VALUES (32, '0', '::1', '2019-05-11 11:25:32');
INSERT INTO `visitor` VALUES (33, '0', '::1', '2019-05-11 11:25:33');
INSERT INTO `visitor` VALUES (34, '0', '::1', '2019-05-11 11:25:36');
INSERT INTO `visitor` VALUES (35, '0', '::1', '2019-05-11 11:25:50');
INSERT INTO `visitor` VALUES (36, '0', '::1', '2019-05-11 11:35:10');
INSERT INTO `visitor` VALUES (37, '0', '::1', '2019-05-11 11:37:15');
INSERT INTO `visitor` VALUES (38, '0', '::1', '2019-05-11 11:38:13');
INSERT INTO `visitor` VALUES (39, '0', '::1', '2019-05-11 11:43:32');
INSERT INTO `visitor` VALUES (40, '0', '::1', '2019-05-11 12:02:50');
INSERT INTO `visitor` VALUES (41, '0', '::1', '2019-05-11 12:02:51');
INSERT INTO `visitor` VALUES (42, '0', '::1', '2019-05-11 12:02:59');
INSERT INTO `visitor` VALUES (43, '0', '::1', '2019-05-11 12:04:13');
INSERT INTO `visitor` VALUES (44, '0', '::1', '2019-05-11 12:05:30');
INSERT INTO `visitor` VALUES (45, '0', '::1', '2019-05-11 12:06:08');
INSERT INTO `visitor` VALUES (46, '0', '::1', '2019-05-11 12:06:34');
INSERT INTO `visitor` VALUES (47, '0', '::1', '2019-05-11 12:06:53');
INSERT INTO `visitor` VALUES (48, '0', '::1', '2019-05-11 12:08:17');
INSERT INTO `visitor` VALUES (49, '0', '::1', '2019-05-11 12:08:23');
INSERT INTO `visitor` VALUES (50, '0', '::1', '2019-05-11 12:09:48');
INSERT INTO `visitor` VALUES (51, '0', '::1', '2019-05-11 12:14:41');
INSERT INTO `visitor` VALUES (52, '0', '::1', '2019-05-11 12:17:07');
INSERT INTO `visitor` VALUES (53, 'www', '::1', '2019-05-12 05:04:12');
INSERT INTO `visitor` VALUES (54, 'www', '::1', '2019-05-12 05:04:14');
INSERT INTO `visitor` VALUES (55, 'www', '::1', '2019-05-12 05:04:47');
INSERT INTO `visitor` VALUES (56, 'www', '::1', '2019-05-12 05:06:46');
INSERT INTO `visitor` VALUES (57, 'www', '::1', '2019-05-12 05:06:47');
INSERT INTO `visitor` VALUES (58, 'www', '::1', '2019-05-12 05:06:58');
INSERT INTO `visitor` VALUES (59, 'www', '::1', '2019-05-12 05:07:36');
INSERT INTO `visitor` VALUES (60, 'www', '::1', '2019-05-12 05:08:50');
INSERT INTO `visitor` VALUES (61, 'www', '::1', '2019-05-12 05:09:01');
INSERT INTO `visitor` VALUES (62, 'www', '::1', '2019-05-12 05:09:27');
INSERT INTO `visitor` VALUES (63, 'www', '::1', '2019-05-12 05:10:04');
INSERT INTO `visitor` VALUES (64, 'www', '::1', '2019-05-12 05:11:51');
INSERT INTO `visitor` VALUES (65, 'www', '::1', '2019-05-12 05:13:03');
INSERT INTO `visitor` VALUES (66, 'www', '::1', '2019-05-12 05:13:59');
INSERT INTO `visitor` VALUES (67, 'www', '::1', '2019-05-12 05:14:00');
INSERT INTO `visitor` VALUES (68, 'www', '::1', '2019-05-12 05:14:01');
INSERT INTO `visitor` VALUES (69, 'www', '::1', '2019-05-12 05:14:35');
INSERT INTO `visitor` VALUES (70, 'www', '::1', '2019-05-12 05:15:44');
INSERT INTO `visitor` VALUES (71, 'www', '::1', '2019-05-12 05:16:32');
INSERT INTO `visitor` VALUES (72, 'www', '::1', '2019-05-12 05:17:42');
INSERT INTO `visitor` VALUES (73, 'www', '::1', '2019-05-12 05:18:28');
INSERT INTO `visitor` VALUES (74, 'www', '::1', '2019-05-12 05:19:18');
INSERT INTO `visitor` VALUES (75, 'www', '::1', '2019-05-12 05:20:08');
INSERT INTO `visitor` VALUES (76, 'www', '::1', '2019-05-12 05:21:16');
INSERT INTO `visitor` VALUES (77, 'www', '::1', '2019-05-12 05:22:06');
INSERT INTO `visitor` VALUES (78, 'www', '::1', '2019-05-12 05:22:56');
INSERT INTO `visitor` VALUES (79, 'www', '::1', '2019-05-12 05:24:01');
INSERT INTO `visitor` VALUES (80, 'www', '::1', '2019-05-12 05:24:47');
INSERT INTO `visitor` VALUES (81, 'www', '::1', '2019-05-12 05:25:07');
INSERT INTO `visitor` VALUES (82, 'www', '::1', '2019-05-12 12:01:36');
INSERT INTO `visitor` VALUES (83, 'www', '::1', '2019-05-12 12:16:24');
INSERT INTO `visitor` VALUES (84, 'www', '::1', '2019-05-12 12:16:34');
INSERT INTO `visitor` VALUES (85, 'www', '::1', '2019-05-12 12:18:12');
INSERT INTO `visitor` VALUES (86, 'www', '::1', '2019-05-12 12:18:34');
INSERT INTO `visitor` VALUES (87, 'www', '::1', '2019-05-12 12:18:34');
INSERT INTO `visitor` VALUES (88, 'www', '::1', '2019-05-12 12:18:35');
INSERT INTO `visitor` VALUES (89, 'www', '::1', '2019-05-12 12:18:38');
INSERT INTO `visitor` VALUES (90, 'www', '::1', '2019-05-12 12:19:42');
INSERT INTO `visitor` VALUES (91, 'www', '::1', '2019-05-12 12:20:03');
INSERT INTO `visitor` VALUES (92, 'www', '::1', '2019-05-12 12:20:04');
INSERT INTO `visitor` VALUES (93, 'www', '::1', '2019-05-12 13:39:27');
INSERT INTO `visitor` VALUES (94, 'www', '::1', '2019-05-12 13:39:33');
INSERT INTO `visitor` VALUES (95, 'www', '::1', '2019-05-12 13:41:50');
INSERT INTO `visitor` VALUES (96, 'www', '::1', '2019-05-12 13:42:04');
INSERT INTO `visitor` VALUES (97, 'www', '::1', '2019-05-12 13:42:05');
INSERT INTO `visitor` VALUES (98, 'www', '::1', '2019-05-12 13:42:09');
INSERT INTO `visitor` VALUES (99, 'www', '::1', '2019-05-12 13:42:16');
INSERT INTO `visitor` VALUES (100, 'www', '::1', '2019-05-12 13:43:22');
INSERT INTO `visitor` VALUES (101, 'www', '::1', '2019-05-12 13:44:29');
INSERT INTO `visitor` VALUES (102, 'www', '::1', '2019-05-12 13:46:09');
INSERT INTO `visitor` VALUES (103, 'www', '::1', '2019-05-12 13:46:28');
INSERT INTO `visitor` VALUES (104, 'www', '::1', '2019-05-12 13:46:58');
INSERT INTO `visitor` VALUES (105, 'www', '::1', '2019-05-12 13:47:10');
INSERT INTO `visitor` VALUES (106, 'www', '::1', '2019-05-12 13:47:32');
INSERT INTO `visitor` VALUES (107, 'www', '::1', '2019-05-12 13:48:18');
INSERT INTO `visitor` VALUES (108, 'www', '::1', '2019-05-12 13:48:25');
INSERT INTO `visitor` VALUES (109, 'www', '::1', '2019-05-12 13:48:32');
INSERT INTO `visitor` VALUES (110, 'www', '::1', '2019-05-12 13:49:58');
INSERT INTO `visitor` VALUES (111, 'www', '::1', '2019-05-12 13:50:02');
INSERT INTO `visitor` VALUES (112, 'www', '::1', '2019-05-12 13:51:03');
INSERT INTO `visitor` VALUES (113, 'www', '::1', '2019-05-12 13:51:04');
INSERT INTO `visitor` VALUES (114, 'www', '::1', '2019-05-12 13:51:06');
INSERT INTO `visitor` VALUES (115, 'www', '::1', '2019-05-12 13:51:06');
INSERT INTO `visitor` VALUES (116, 'www', '::1', '2019-05-12 13:51:07');
INSERT INTO `visitor` VALUES (117, 'www', '::1', '2019-05-12 13:51:08');
INSERT INTO `visitor` VALUES (118, 'www', '::1', '2019-05-12 13:51:09');
INSERT INTO `visitor` VALUES (119, 'www', '::1', '2019-05-12 13:51:10');
INSERT INTO `visitor` VALUES (120, 'www', '::1', '2019-05-12 13:51:37');
INSERT INTO `visitor` VALUES (121, 'www', '::1', '2019-05-12 13:51:45');
INSERT INTO `visitor` VALUES (122, 'www', '::1', '2019-05-12 13:52:02');
INSERT INTO `visitor` VALUES (123, 'www', '::1', '2019-05-12 13:52:27');
INSERT INTO `visitor` VALUES (124, 'www', '::1', '2019-05-12 13:52:28');
INSERT INTO `visitor` VALUES (125, 'www', '::1', '2019-05-12 13:52:29');
INSERT INTO `visitor` VALUES (126, 'www', '::1', '2019-05-12 13:53:00');
INSERT INTO `visitor` VALUES (127, 'www', '::1', '2019-05-12 13:53:01');
INSERT INTO `visitor` VALUES (128, 'www', '::1', '2019-05-12 13:53:01');
INSERT INTO `visitor` VALUES (129, 'www', '::1', '2019-05-12 13:53:01');
INSERT INTO `visitor` VALUES (130, 'www', '::1', '2019-05-12 13:53:01');
INSERT INTO `visitor` VALUES (131, 'www', '::1', '2019-05-12 13:53:10');
INSERT INTO `visitor` VALUES (132, 'www', '::1', '2019-05-12 13:53:27');
INSERT INTO `visitor` VALUES (133, 'www', '::1', '2019-05-12 13:53:28');
INSERT INTO `visitor` VALUES (134, 'www', '::1', '2019-05-12 13:53:39');
INSERT INTO `visitor` VALUES (135, 'www', '::1', '2019-05-12 13:53:41');
INSERT INTO `visitor` VALUES (136, 'www', '::1', '2019-05-12 13:59:55');
INSERT INTO `visitor` VALUES (137, 'www', '::1', '2019-05-12 14:06:54');
INSERT INTO `visitor` VALUES (138, 'www', '::1', '2019-05-12 14:07:04');
INSERT INTO `visitor` VALUES (139, 'www', '::1', '2019-05-12 14:07:29');
INSERT INTO `visitor` VALUES (140, 'www', '::1', '2019-05-12 14:07:35');
INSERT INTO `visitor` VALUES (141, 'www', '::1', '2019-05-12 14:08:22');
INSERT INTO `visitor` VALUES (142, 'www', '::1', '2019-05-12 14:08:31');
INSERT INTO `visitor` VALUES (143, 'www', '::1', '2019-05-12 14:11:26');
INSERT INTO `visitor` VALUES (144, 'www', '::1', '2019-05-12 14:11:36');
INSERT INTO `visitor` VALUES (145, 'www', '::1', '2019-05-12 14:12:00');
INSERT INTO `visitor` VALUES (146, 'www', '::1', '2019-05-12 14:12:01');
INSERT INTO `visitor` VALUES (147, 'www', '::1', '2019-05-12 14:12:16');
INSERT INTO `visitor` VALUES (148, 'www', '::1', '2019-05-12 14:12:35');
INSERT INTO `visitor` VALUES (149, 'www', '::1', '2019-05-12 14:14:10');
INSERT INTO `visitor` VALUES (150, 'www', '::1', '2019-05-12 14:14:14');
INSERT INTO `visitor` VALUES (151, 'www', '::1', '2019-05-12 14:14:27');
INSERT INTO `visitor` VALUES (152, 'www', '::1', '2019-05-12 14:15:10');
INSERT INTO `visitor` VALUES (153, 'www', '::1', '2019-05-12 14:15:40');
INSERT INTO `visitor` VALUES (154, 'www', '::1', '2019-05-12 14:15:42');
INSERT INTO `visitor` VALUES (155, 'www', '::1', '2019-05-12 14:15:53');
INSERT INTO `visitor` VALUES (156, 'www', '::1', '2019-05-12 14:17:29');
INSERT INTO `visitor` VALUES (157, 'www', '::1', '2019-05-12 14:18:21');
INSERT INTO `visitor` VALUES (158, 'www', '::1', '2019-05-12 14:20:01');
INSERT INTO `visitor` VALUES (159, 'www', '::1', '2019-05-12 14:20:46');
INSERT INTO `visitor` VALUES (160, 'www', '::1', '2019-05-12 14:37:46');
INSERT INTO `visitor` VALUES (161, 'www', '::1', '2019-05-12 14:39:22');
INSERT INTO `visitor` VALUES (162, 'www', '::1', '2019-05-12 14:39:27');
INSERT INTO `visitor` VALUES (163, 'www', '::1', '2019-05-12 21:57:38');
INSERT INTO `visitor` VALUES (164, 'www', '::1', '2019-05-12 21:59:02');
INSERT INTO `visitor` VALUES (165, 'www', '::1', '2019-05-12 21:59:14');
INSERT INTO `visitor` VALUES (166, 'www', '::1', '2019-05-12 22:00:49');
INSERT INTO `visitor` VALUES (167, 'www', '::1', '2019-05-12 22:02:42');
INSERT INTO `visitor` VALUES (168, 'www', '::1', '2019-05-12 22:02:42');
INSERT INTO `visitor` VALUES (169, 'www', '::1', '2019-05-12 22:02:48');
INSERT INTO `visitor` VALUES (170, 'www', '::1', '2019-05-12 22:12:06');
INSERT INTO `visitor` VALUES (171, 'www', '::1', '2019-05-12 22:12:56');
INSERT INTO `visitor` VALUES (172, 'www', '::1', '2019-05-12 22:22:07');
INSERT INTO `visitor` VALUES (173, 'www', '::1', '2019-05-12 22:31:27');
INSERT INTO `visitor` VALUES (174, 'www', '::1', '2019-05-12 22:31:59');
INSERT INTO `visitor` VALUES (175, 'www', '::1', '2019-05-12 22:32:27');
INSERT INTO `visitor` VALUES (176, 'www', '::1', '2019-05-12 22:32:39');
INSERT INTO `visitor` VALUES (177, 'www', '::1', '2019-05-13 09:56:47');
INSERT INTO `visitor` VALUES (178, 'www', '::1', '2019-05-13 10:33:02');
INSERT INTO `visitor` VALUES (179, 'www', '::1', '2019-05-13 10:34:14');
INSERT INTO `visitor` VALUES (180, 'www', '::1', '2019-05-13 10:36:15');
INSERT INTO `visitor` VALUES (181, 'www', '::1', '2019-05-13 10:36:21');
INSERT INTO `visitor` VALUES (182, 'www', '::1', '2019-05-13 10:36:29');
INSERT INTO `visitor` VALUES (183, 'www', '::1', '2019-05-13 10:36:59');
INSERT INTO `visitor` VALUES (184, 'www', '::1', '2019-05-13 10:37:06');
INSERT INTO `visitor` VALUES (185, 'www', '::1', '2019-05-13 10:37:36');
INSERT INTO `visitor` VALUES (186, 'www', '::1', '2019-05-13 10:37:38');
INSERT INTO `visitor` VALUES (187, 'www', '::1', '2019-05-13 10:37:40');
INSERT INTO `visitor` VALUES (188, 'www', '::1', '2019-05-13 10:37:44');
INSERT INTO `visitor` VALUES (189, 'www', '::1', '2019-05-13 10:37:50');
INSERT INTO `visitor` VALUES (190, 'www', '::1', '2019-05-13 10:38:10');
INSERT INTO `visitor` VALUES (191, 'www', '::1', '2019-05-13 10:38:12');
INSERT INTO `visitor` VALUES (192, 'www', '::1', '2019-05-13 10:38:14');
INSERT INTO `visitor` VALUES (193, 'www', '::1', '2019-05-13 10:38:14');
INSERT INTO `visitor` VALUES (194, 'www', '::1', '2019-05-13 10:38:30');
INSERT INTO `visitor` VALUES (195, 'www', '::1', '2019-05-13 10:38:36');
INSERT INTO `visitor` VALUES (196, 'www', '::1', '2019-05-13 10:38:37');
INSERT INTO `visitor` VALUES (197, 'www', '::1', '2019-05-13 10:38:38');
INSERT INTO `visitor` VALUES (198, 'www', '::1', '2019-05-13 10:38:52');
INSERT INTO `visitor` VALUES (199, 'www', '::1', '2019-05-13 10:38:52');
INSERT INTO `visitor` VALUES (200, 'www', '::1', '2019-05-13 10:38:52');
INSERT INTO `visitor` VALUES (201, 'www', '::1', '2019-05-13 10:38:56');
INSERT INTO `visitor` VALUES (202, 'www', '::1', '2019-05-13 10:40:17');
INSERT INTO `visitor` VALUES (203, 'www', '::1', '2019-05-13 10:40:19');
INSERT INTO `visitor` VALUES (204, 'www', '::1', '2019-05-13 10:40:20');
INSERT INTO `visitor` VALUES (205, 'www', '::1', '2019-05-13 10:43:30');
INSERT INTO `visitor` VALUES (206, 'www', '::1', '2019-05-13 10:45:38');
INSERT INTO `visitor` VALUES (207, 'www', '::1', '2019-05-13 10:45:39');
INSERT INTO `visitor` VALUES (208, 'www', '::1', '2019-05-13 10:45:45');
INSERT INTO `visitor` VALUES (209, 'www', '::1', '2019-05-13 10:45:48');
INSERT INTO `visitor` VALUES (210, 'www', '::1', '2019-05-13 10:50:21');
INSERT INTO `visitor` VALUES (211, 'www', '::1', '2019-05-13 10:51:17');
INSERT INTO `visitor` VALUES (212, 'www', '::1', '2019-05-13 10:51:22');
INSERT INTO `visitor` VALUES (213, 'www', '::1', '2019-05-13 10:55:12');
INSERT INTO `visitor` VALUES (214, 'www', '::1', '2019-05-13 10:55:30');
INSERT INTO `visitor` VALUES (215, 'www', '::1', '2019-05-13 11:00:34');
INSERT INTO `visitor` VALUES (216, 'www', '::1', '2019-05-13 11:01:48');
INSERT INTO `visitor` VALUES (217, 'www', '::1', '2019-05-13 11:11:07');
INSERT INTO `visitor` VALUES (218, 'www', '::1', '2019-05-13 11:12:21');
INSERT INTO `visitor` VALUES (219, 'www', '::1', '2019-05-13 11:15:48');
INSERT INTO `visitor` VALUES (220, 'www', '::1', '2019-05-13 11:16:17');
INSERT INTO `visitor` VALUES (221, 'www', '::1', '2019-05-13 11:28:46');
INSERT INTO `visitor` VALUES (222, 'www', '::1', '2019-05-13 11:31:08');
INSERT INTO `visitor` VALUES (223, 'www', '::1', '2019-05-13 12:05:29');
INSERT INTO `visitor` VALUES (224, 'www', '::1', '2019-05-13 12:05:44');
INSERT INTO `visitor` VALUES (225, 'www', '::1', '2019-05-13 12:06:09');
INSERT INTO `visitor` VALUES (226, 'www', '::1', '2019-05-13 12:09:09');
INSERT INTO `visitor` VALUES (227, 'www', '::1', '2019-05-13 12:09:21');
INSERT INTO `visitor` VALUES (228, 'www', '::1', '2019-05-13 12:09:55');
INSERT INTO `visitor` VALUES (229, 'www', '::1', '2019-05-13 12:09:56');
INSERT INTO `visitor` VALUES (230, 'www', '::1', '2019-05-13 12:11:04');
INSERT INTO `visitor` VALUES (231, 'www', '::1', '2019-05-13 12:11:06');
INSERT INTO `visitor` VALUES (232, 'www', '::1', '2019-05-13 12:11:07');
INSERT INTO `visitor` VALUES (233, 'www', '::1', '2019-05-13 12:11:08');
INSERT INTO `visitor` VALUES (234, 'www', '::1', '2019-05-13 12:11:08');
INSERT INTO `visitor` VALUES (235, 'www', '::1', '2019-05-13 12:24:06');
INSERT INTO `visitor` VALUES (236, 'www', '::1', '2019-05-13 12:24:14');
INSERT INTO `visitor` VALUES (237, 'www', '::1', '2019-05-13 12:28:23');
INSERT INTO `visitor` VALUES (238, 'www', '::1', '2019-05-13 12:30:36');
INSERT INTO `visitor` VALUES (239, 'www', '::1', '2019-05-13 12:32:20');
INSERT INTO `visitor` VALUES (240, 'www', '::1', '2019-05-13 12:45:31');
INSERT INTO `visitor` VALUES (241, 'www', '::1', '2019-05-13 12:46:13');
INSERT INTO `visitor` VALUES (242, 'www', '::1', '2019-05-13 12:47:35');
INSERT INTO `visitor` VALUES (243, 'www', '::1', '2019-05-13 12:49:13');
INSERT INTO `visitor` VALUES (244, 'www', '::1', '2019-05-13 12:49:31');
INSERT INTO `visitor` VALUES (245, 'www', '::1', '2019-05-13 12:49:41');
INSERT INTO `visitor` VALUES (246, 'www', '::1', '2019-05-13 12:50:33');
INSERT INTO `visitor` VALUES (247, 'www', '::1', '2019-05-13 12:51:29');
INSERT INTO `visitor` VALUES (248, 'www', '::1', '2019-05-13 12:51:30');
INSERT INTO `visitor` VALUES (249, 'www', '::1', '2019-05-13 12:51:40');
INSERT INTO `visitor` VALUES (250, 'www', '::1', '2019-05-13 12:52:33');
INSERT INTO `visitor` VALUES (251, 'www', '::1', '2019-05-13 12:52:51');
INSERT INTO `visitor` VALUES (252, 'www', '::1', '2019-05-13 12:53:07');
INSERT INTO `visitor` VALUES (253, 'www', '::1', '2019-05-13 15:40:04');
INSERT INTO `visitor` VALUES (254, 'www', '::1', '2019-05-13 16:12:04');
INSERT INTO `visitor` VALUES (255, 'www', '::1', '2019-05-13 16:20:27');
INSERT INTO `visitor` VALUES (256, 'www', '::1', '2019-05-13 16:22:38');
INSERT INTO `visitor` VALUES (257, 'www', '::1', '2019-05-13 16:23:26');
INSERT INTO `visitor` VALUES (258, 'www', '::1', '2019-05-13 16:23:28');
INSERT INTO `visitor` VALUES (259, 'www', '::1', '2019-05-13 16:28:49');
INSERT INTO `visitor` VALUES (260, 'www', '::1', '2019-05-13 16:28:50');
INSERT INTO `visitor` VALUES (261, 'www', '::1', '2019-05-13 16:28:52');
INSERT INTO `visitor` VALUES (262, 'www', '::1', '2019-05-13 16:29:42');
INSERT INTO `visitor` VALUES (263, 'www', '::1', '2019-05-13 16:30:24');
INSERT INTO `visitor` VALUES (264, 'www', '::1', '2019-05-13 16:31:15');
INSERT INTO `visitor` VALUES (265, 'www', '::1', '2019-05-13 16:40:28');
INSERT INTO `visitor` VALUES (266, 'www', '::1', '2019-05-13 16:49:35');
INSERT INTO `visitor` VALUES (267, 'www', '::1', '2019-05-13 16:49:37');
INSERT INTO `visitor` VALUES (268, 'www', '::1', '2019-05-13 17:03:32');
INSERT INTO `visitor` VALUES (269, 'www', '::1', '2019-05-13 17:03:57');
INSERT INTO `visitor` VALUES (270, 'www', '::1', '2019-05-13 17:05:21');
INSERT INTO `visitor` VALUES (271, 'www', '::1', '2019-05-15 13:40:48');
INSERT INTO `visitor` VALUES (272, '0', '::1', '2019-05-15 13:42:02');
INSERT INTO `visitor` VALUES (273, 'www', '::1', '2019-05-15 13:42:03');
INSERT INTO `visitor` VALUES (274, 'www', '::1', '2019-05-15 13:42:05');
COMMIT;

-- ----------------------------
-- View structure for v_section_type_list
-- ----------------------------
DROP VIEW IF EXISTS `v_section_type_list`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_section_type_list` AS select `l`.`id` AS `id`,`l`.`directory` AS `directory`,`t`.`menu` AS `menu`,`l`.`section_id` AS `section_id`,`l`.`type_sec_id` AS `type_sec_id`,`l`.`sec_order` AS `sec_order`,`l`.`date_created` AS `date_created`,`t`.`db_table` AS `db_table`,`t`.`name` AS `name` from (`section_list` `l` join `section_type` `t` on((`l`.`type_sec_id` = `t`.`id`)));

SET FOREIGN_KEY_CHECKS = 1;
