<?php

$lang['required']			= "Kolom %s harus terisi.";
$lang['isset']				= "Kolom %s harus terisi minimal 1 karakter.";
$lang['valid_email']		= "Kolom %s harus berisi alamat email yang benar.";
$lang['valid_emails']		= "Kolom %s harus berisi alamat email yang benar.";
$lang['valid_url']			= "Kolom %s harus berisi alamat URL yang benar.";
$lang['valid_ip']			= "Kolom %s harus berisi contain a valid IP.";
$lang['min_length']			= "Kolom %s harus berisi be at least %s characters in length.";
$lang['max_length']			= "Kolom %s harus berisi maksimal sebanyak %s karakter.";
$lang['exact_length']		= "Kolom %s harus berisi persis sejumlah %s karakter.";
$lang['alpha']				= "Kolom %s hanya boleh diisi dengan huruf.";
$lang['alpha_numeric']		= "Kolom %s hanya boleh diisi kombinasi angka dan huruf.";
$lang['alpha_dash']			= "Kolom %s hanya boleh diisi huruf, tanda baca underscores (_) dan dashes (-)";
$lang['numeric']			= "Kolom %s harus berisi angka saja.";
$lang['is_numeric']			= "Kolom %s harus berisi contain only numeric characters.";
$lang['integer']			= "Kolom %s harus berisi contain an integer.";
$lang['regex_match']		= "Kolom %s field is not in the correct format.";
$lang['matches']			= "Kolom %s field does not match the %s field.";
$lang['is_unique'] 			= "Kolom %s harus berisi contain a unique value.";
$lang['is_natural']			= "Kolom %s harus berisi contain only positive numbers.";
$lang['is_natural_no_zero']	= "Kolom %s harus berisi contain a number greater than zero.";
$lang['decimal']			= "Kolom %s harus berisi contain a decimal number.";
$lang['less_than']			= "Kolom %s harus berisi contain a number less than %s.";
$lang['greater_than']		= "Kolom %s harus berisi contain a number greater than %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */